import spacy
import re

# Spell Checker
#from spellchecker import SpellChecker
from textblob import Word

# Profanity Checker
from profanity_check import predict, predict_prob

# Keywords Checker
from Keyword_Extraction import TextRank4Keyword

from pymongo import MongoClient

from Database_Tools import DatabaseTools


class TextProcessing():

    def __init__(self):

        self.text = ""
        self.nlp = spacy.load("en_core_web_lg")
        self.spacy_doc = []
        self.list_of_tokens = []

        self.correction_dict = {}
        self.validate_argument = True

        self.misspelled_words = []
        self.misspelled_words_str = []

        self.profane_words = []
        self.profane_words_str = []
        self.profanity_checked_word_list = []

        self.key_words = []
        self.key_words_str = []

        self.debate_title = ""
        self.similar_arguments = []
        self.similar_arguments_str = []

        '''nlp.Defaults.stop_words |= {"stop","word",}'''  # Adding stopwords if needed
        '''nlp.Defaults.stop_words -= {"stop", "word"}'''  # Removing stopwords if needed

    def process_text(self, text):
        doc = self.nlp(text.lower())
        result = []
        for token in doc:
            if token.text in self.nlp.Defaults.stop_words:
                continue
            if token.is_punct:
                continue
            if token.lemma_ == '-PRON-':
                continue
            result.append(token.lemma_)
        return " ".join(result)

    def reduce_lengthening(self, text):
        pattern = re.compile(r"(.)\1{2,}")
        return pattern.sub(r"\1\1", text)

    def spell_checking_argument(self):

        # enumerate gives index of each token
        for index, token in enumerate(self.spacy_doc):

            reduced_word = self.reduce_lengthening(
                token.text)  # reduce "laaaaa" into "laa"
            word = Word(reduced_word)
            correction_attempts = word.spellcheck()
            # only take first 3 answers
            correction_attempts = correction_attempts[:3]
            correction_attempts_list = []

            for x in correction_attempts:
                correction_attempts_list.append([x[0], round(x[1], 2)])

            if len(correction_attempts) != 1:

                if token.text not in self.misspelled_words:
                    self.misspelled_words.append(token.text)
                    #self.list_correction_words.append([token.text, correction_attempts, index])
                    self.misspelled_words_str.append(
                        "Written word : " + token.text + "  -  Correction attempts : " + str(correction_attempts_list))

    def profanity_checking(self):

        # enumerate gives index of each token
        for index, token in enumerate(self.spacy_doc):

            reduced_word = self.reduce_lengthening(token.text)

            test_list = []  # need to use a list for the predict_prob function
            test_list.append(reduced_word)

            prediction = predict_prob(test_list)

            if prediction >= 0.3:

                self.profane_words.append(token.text)
                self.profanity_checked_word_list.append(
                    [token.text, prediction[0], index])

    def keywords_checking(self):

        tr4w = TextRank4Keyword()
        tr4w.analyze(self.text, candidate_pos=[
                     'NOUN', 'PROPN'], window_size=4, lower=False)

        self.key_words = tr4w.get_keywords(3)

        for key in self.key_words:

            self.key_words_str.append(str(key))

    def entities_checking(self, text):

        entities_list = []
        spacy_doc = self.nlp(text)

        for index, ent in enumerate(spacy_doc.ents):

            entities_list.append([str(ent), index])

        return entities_list

    def similarities_checking(self):

        database_tools = DatabaseTools()
        comparing_text_list = database_tools.database_find_similar_arguments(
            self.debate_title)

        for comparing_text in comparing_text_list:

            #comparing_text = self.process_text(comparing_text[1])
            comparing_grade = self.spacy_doc.similarity(
                self.nlp(comparing_text))

            print(comparing_grade)

            if comparing_grade > 0.01:

                self.similar_arguments.append(
                    [comparing_text, comparing_grade])
                self.similar_arguments_str.append(
                    comparing_text + "  -  Comparison grade = " + str(comparing_grade))

    def rewrite_text_into_span(self, text):

        for correction in self.corrected_words_list:

            text = text.replace(str(correction[0]), "<span dict = spell_checking index = " + str(
                correction[2]) + ">" + str(correction[0]) + "</span>")

        for correction in self.correction_dict["profanitychecked_words"]:

            text = text.replace(str(correction[0]), "<span dict = profanity_checking index = " + str(
                correction[2]) + ">" + str(correction[0]) + "</span>")

        return text

    def rewrite_text_with_colors(self):

        new_token_list = self.list_of_tokens

        for correction in self.corrected_words_list:

            corrected_str = '<Text style={{color: "yellow"}}>' + \
                str(correction[0]) + '</Text>'
            new_token_list = [corrected_str if x ==
                              correction[0] else x for x in self.list_of_tokens]

        for correction in self.profanity_checked_words_list:

            corrected_str = '<Text style={{color: "red"}}>' + \
                str(correction[0]) + '</Text>'
            new_token_list = [corrected_str if x ==
                              correction[0] else x for x in self.list_of_tokens]

        return new_token_list
    '''-------------------------------------------------------------------------------------------------------------------'''

    def checking_new_argument(self, text, debat_info):

        self.text = text
        self.spacy_doc = self.nlp(text)
        self.list_of_tokens = [i.text for i in self.spacy_doc]

        self.correction_dict["original_text"] = self.text

        #processed_text = self.process_text(text)

        self.spell_checking_argument()
        self.correction_dict["misspelledWords"] = self.misspelled_words
        self.correction_dict["misspelledWordsStr"] = self.misspelled_words_str

        self.profanity_checking()
        self.correction_dict["profaneWords"] = self.profane_words

        self.keywords_checking()
        self.correction_dict["keyWords"] = self.key_words
        self.correction_dict["keyWordsStr"] = self.key_words_str

        #self.correction_dict["entities"] = self.entities_checking(text)

        self.debate_title = debat_info

        print(self.debate_title)

        self.similarities_checking()
        #self.correction_dict["similarArgs"] = self.similar_arguments
        self.correction_dict["similarArgs"] = self.similar_arguments_str

        if self.profane_words:  # If list not empty
            self.validate_argument = False

        self.correction_dict["argumentValid?"] = self.validate_argument

        self.correction_dict["finalText"] = self.list_of_tokens

        return self.correction_dict

    def checking_new_debate(self, text):

        self.text = text
        self.spacy_doc = self.nlp(text)
        self.list_of_tokens = [i.text for i in self.spacy_doc]

        self.correction_dict["original_text"] = self.text

        self.spell_checking_argument()
        self.correction_dict["misspelled_words"] = self.misspelled_words
        self.correction_dict["list_correction_words"] = self.list_correction_words

        self.profanity_checking()
        self.correction_dict["profane_words"] = self.profane_words

        if self.profane_words:  # If list not empty
            self.validate_argument = False

        self.correction_dict["validate_argument"] = self.validate_argument

        self.correction_dict["final_text"] = self.list_of_tokens

        return self.correction_dict

    def main(self, text, debat_info):
        self.checking_new_argument(text, debat_info)
        print(self.correction_dict["similarArgs"])


TextProcessing().main("I disagree with that.", "5e26a86e82c9e3161c3a7f37")
