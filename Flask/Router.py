from flask import Flask, redirect, url_for, request, jsonify
from Text_Processing import TextProcessing
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

# /login for  testing purposes
@app.route('/login', methods=['POST', 'GET'])
def login():
    if request.method == 'POST':

        text_processer = TextProcessing()
        correction_dict = text_processer.checking_new_argument(
            request.form['user_text'], "empty")
        return jsonify(correction_dict)

    else:

        user = request.args.get('user_text')
        return redirect(url_for('success', name=user))


@app.route('/correct_comment', methods=['POST', 'GET'])
def correction_text():

    if request.method == 'POST':

        text_processer = TextProcessing()
        correction_dict = text_processer.checking_new_argument(
            request.json["user_input"], request.json["debate"])
        return jsonify(correction_dict)

    else:

        user = request.args.get('user_text')
        return redirect(url_for('success', name=user))


@app.route('/correct_debat', methods=['POST', 'GET'])
def correction_debat():

    if request.method == 'POST':

        text_processer = TextProcessing()
        correction_dict = text_processer.checking_new_debate(
            request.json["content"])
        return jsonify(correction_dict)

    else:

        user = request.args.get('user_text')
        return redirect(url_for('success', name=user))


@app.route('/get_keywords', methods=['POST', 'GET'])
def get_keywords():

    if request.method == 'POST':

        text_processer = TextProcessing()
        keywords_dict = text_processer.keywords_checking(
            request.json["user_text"])
        return jsonify(keywords_dict)

    '''else:

        user = request.args.get('user_text')
        return redirect(url_for('success', name=user))'''


if __name__ == '__main__':
    app.run(debug=True)
