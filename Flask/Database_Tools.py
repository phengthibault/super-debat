from pymongo import MongoClient


class DatabaseTools():

    def __init__(self):

        self.client = MongoClient(
            "mongodb+srv://admin:admin@cluster0-nok70.mongodb.net/test?retryWrites=true&w=majority")
        self.db = self.client.databaseProjet

    def database_find_similar_arguments(self, debat_info):

        self.collection = self.db["ArgModel"]

        query = {
            "idDebat": debat_info
        }

        '''fields = {
            "_id": 0, "argsFor": 1, "argsAgainst": 1
        }'''

        fields = {
            "_id": 0, "content": 1
        }

        doc = self.collection.find(query, fields)

        elems = [x["content"] for x in doc]

        '''args_for = elems[0][0]
        args_against = elems[0][1]'''

        '''list_args = []

        for x in args_for:
            list_args.append(x["content"])

        for x in args_against:
            list_args.append(x["content"])'''

        return elems
