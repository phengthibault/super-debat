import spacy
from spacy import displacy

nlp = spacy.load("en_core_web_sm")

def displacy_service(text):
    doc = nlp(text)
    return displacy.parse_deps(doc)

displacy("Try with this first text")