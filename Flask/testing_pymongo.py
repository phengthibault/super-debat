from pymongo import MongoClient

client = MongoClient("mongodb+srv://admin:admin@cluster0-nok70.mongodb.net/test?retryWrites=true&w=majority")
db = client.databaseProjet

collection = db["DebatModel"]
query = {"keyWords" : "retraite"}
fields = {
    "_id" : 1
    ,"content" : 1
    #,"keyWords" : 0
    #,"numberLikes" : 0
    #,"numberDislikes" : 0
    #,"idauthorLogin" : 0
    #,"inFavor" : 0
    #,"dateFormat" : 0
    #,"_class" : 0
}  

'''insert_dict = {
    "content" : "This content will be used for the similarity filter"
    ,"keyWords" : "content"
    ,"numberLikes" : 12
    ,"numberDislikes" : 16
    ,"idauthorLogin" : 1
    ,"inFavor" : True
    ,"dateFormat" : "01-10-2020"
    ,"_class" : "com.cpe.springboot.arg.model.ArgModel"
}   '''

insert_dict = {
"title"  :"Grand Débat : Quel est aujourd'hui pour vous le problème concret le plus important dans le domaine de l'environnement ?"
,"idCreator" : 1
,"numberLikes" : 0
,"numberDislikes" : 0
,"idtopArg" : 5
,"date"  : "14-01-2020"
,"imgUrl": "urlimg"
,"imgTitle" : "imgTitle"
,"describe" : "Première question du Grand Débat dans la catégorie 'Transition Ecologique'"
,"_class" : "com.cpe.springboot.debat.model.DebatModel"
}   

#doc = collection.find(query,fields)

collection.insert_one(insert_dict)

    