let urlBack = "http://localhost:8082/";

export const GET_DEBATE_FROM_ID = urlBack + "Debat/";
export const GET_TOP_DEBATES = urlBack + "Debats";
export const UPDATE_LIKE_ARG = urlBack + "ArgUpdateLike/"; ///DebatUpdateLike/{idDebat}/{idLiker}
export const UPDATE_DISLIKE_ARG = urlBack + "ArgUpdateDisLike/";
export const UPDATE_LIKE_DEBATE = urlBack + "DebatUpdateLike/";
export const UPDATE_DISLIKE_DEBATE = urlBack + "DebatUpdateDisLike/";
export const CHECK_ARGUMENT = urlBack + "CHECK_ARGUMENT";
export const UPDATE_PROFILE_FROM_ID = urlBack + "user/";
export const GET_ARGSFOR_FROM_DEBATE_ID = urlBack + "DebatArgsFor/";
export const GET_ARGSAGAINST_FROM_DEBATE_ID = urlBack + "DebatArgsAgainst/";
export const GET_USER_FROM_ID = urlBack + "user/";
export const POST_DEBATE = urlBack + "Debat";

export const DELETE_OPINION = urlBack + "Arg/";
export const DELETE_DEBATE = urlBack + "Debat/";
export const POST_ARGUMENT = urlBack + "Arg";
export const GET_SORTED_ARGSFOR_HOT = urlBack + "SortedArgsForHOT/";
export const GET_SORTED_ARGSAGAINST_HOT = urlBack + "SortedArgsAgainstHOT/";
export const GET_SORTED_ARGSAGAINST_FRESH = urlBack + "SortedArgsAgainstFRESH/";
export const GET_SORTED_ARGSFOR_FRESH = urlBack + "SortedArgsForFRESH/";
export const SET_FACT_CHECKING = urlBack + "FactCheck/";
export const GET_KEYWORDS_SEARCHBAR = urlBack + "Debats/keyWords";
export const GET_NEW_DEBATES_SEARCHBAR = urlBack + "getDebats/";
export const GET_COMMENT_USER = urlBack + "GetArgsOfUser/";
