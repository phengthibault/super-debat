import { createStore } from "redux";
import reducer from "../Reducers/reducer";
import {
  devToolsEnhancer,
  composeWithDevTools
} from "redux-devtools-extension";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage"; // defaults to localStorage for web
import { createMigrate } from "redux-persist";

/*const migrations = {
  0: state => {
    // migration clear out device state
    return {
      ...state,
      device: undefined
    };
  },
  1: state => {
    // migration to keep only device state
    return {
      device: state.device
    };
  }
};

const MIGRATION_DEBUG = false;

const persistConfig = {
  key: "persistedStore",
  version: 5,
  storage,
  migrate: createMigrate(migrations, { debug: MIGRATION_DEBUG })
};

const persistedReducer = persistReducer(persistConfig, reducer);
export const store = createStore(persistedReducer, devToolsEnhancer());*/
const composeEnhancers = composeWithDevTools({
  trace: true,
  traceLimit: 25
});
export const store = createStore(reducer, composeEnhancers());
//export const persistor = persistStore(store);
