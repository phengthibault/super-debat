import React, { Component } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { red, green } from "@material-ui/core/colors";
import Avatar from "@material-ui/core/Avatar";
import { connect } from "react-redux";
import ProfileIcon from "./profileIcon";
import InteractBar from "./interactBar";
import { against_color, for_color, middle_color } from "../CSS/color";

function mapStateToProps(state) {
  return { selectedDebate: state.selectedDebate };
}

const useStyles = makeStyles(theme => ({
  flexContainer: { display: "flex", minHeight: "25vh" },
  sides: {
    flexGrow: "1",
    textAlign: "center",
    "border-radius": "25px",
    margin: "5px"
  },
  middle: {
    flexGrow: "2",
    textAlign: "center",
    width: "70%",
    backgroundColor: middle_color,
    "border-radius": "25px",
    margin: "5px"
  },
  center: { margin: "5vh auto" },
  againstSideContainer: {
    backgroundColor: for_color
  },
  forSideContainer: {
    backgroundColor: against_color
  },
  title: { display: "inline-block" },
  creator: { display: "inline-block", float: "left", position: "relative" }
}));

function ConnectedHeaderDebate(props) {
  const classes = useStyles();
  return (
    <div className={classes.flexContainer}>
      <div className={`${classes.sides} ${classes.againstSideContainer}`}>
        <Avatar className={classes.center}>
          {props.selectedDebate.percentageFor}%
        </Avatar>
        <span>Keywords : {props.selectedDebate.keywordsFor}</span>
      </div>
      <div className={classes.middle}>
        <div>
          <ProfileIcon
            className={classes.creator}
            currentUserMode={false}
            user={props.selectedDebate.autor}
            position="absolute"
          />
          <h2 className={classes.title}>{props.selectedDebate.title}</h2>
          <InteractBar
            content={props.selectedDebate}
            type="debate"
          ></InteractBar>
        </div>

        <span>Describe : {props.selectedDebate.describe}</span>
      </div>
      <div className={`${classes.sides} ${classes.forSideContainer}`}>
        <Avatar className={classes.center}>
          {props.selectedDebate.percentageAgainst}%
        </Avatar>
        <span>Keywords : {props.selectedDebate.keywordsAgainst}</span>
      </div>
    </div>
  );
}

const HeaderDebate = connect(mapStateToProps)(ConnectedHeaderDebate);
export default HeaderDebate;
