import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import IconButton from "@material-ui/core/IconButton";
import PhotoCamera from "@material-ui/icons/PhotoCamera";
import ThumbUpIcon from "@material-ui/icons/ThumbUp";
import ThumbDownIcon from "@material-ui/icons/ThumbDown";
import DeleteForeverIcon from "@material-ui/icons/DeleteForever";
import { connect } from "react-redux";
import PolicyIcon from "@material-ui/icons/Policy";
import {
  UPDATE_LIKE_ARG,
  UPDATE_DISLIKE_ARG,
  UPDATE_LIKE_DEBATE,
  UPDATE_DISLIKE_DEBATE,
  DELETE_OPINION,
  DELETE_DEBATE
} from "../Constants/urlApi";
import updateLike from "../Actions/updateLike";
import FactChecking from "./FactChecking";
import { setArgsPour, setArgsContre } from "../Actions/setArgs";
import { buttons_color } from "../CSS/color";

function mapStateToProps(state) {
  return {
    currentUser: state.user,
    isUserLog: state.isUserLog,
    argsFor: state.argsFor,
    argsAgainst: state.argsAgainst
  };
}

function mapDispatchToProps(dispatch) {
  return {
    updateLikeStore: arg => dispatch(updateLike(arg)),
    setArgsFor: arg => dispatch(setArgsPour(arg)),
    setArgsAgainst: arg => dispatch(setArgsContre(arg))
  };
}

var axios = require("axios");

const useStyles = makeStyles(theme => ({
  button: {
    margin: theme.spacing(1),
    color: buttons_color
  },
  likeButton: {
    margin: theme.spacing(1)
  },
  input: {
    display: "none"
  },
  inline: { display: "inline-block", float: "right" }
}));

function deleteContent(props) {
  console.log(props);
  let url = props.type == "debate" ? DELETE_DEBATE : DELETE_OPINION;
  url = url + props.content.id;

  axios
    .delete(url)
    .then(function(response) {
      let argsFor = [];
      let argsAgainst = [];
      if (true) {
        for (var i = 0; i < props.argsFor.length; i++) {
          console.log(props.argsFor[i].id, props.content.id);
          if (props.argsFor[i].id != props.content.id) {
            console.log("different");
            argsFor.push(props.argsFor[i]);
          }
        }
        if (argsFor.length === props.argsFor.length) {
          for (var i = 0; i < props.argsAgainst.length; i++) {
            if (props.argsAgainst[i].id != props.content.id) {
              argsAgainst.push(props.argsAgainst[i]);
            }
          }
          props.setArgsAgainst(argsAgainst);
        } else {
          props.setArgsFor(argsFor);
        }
        console.log(props);
      } else {
        alert("error", response);
      }
    })
    .catch(function(error) {
      console.log(error);
    });
}

function displayAutorObjects(props, classes) {
  if (props.isUserLog) {
    console.log(props);
    if (props.content.autor !== undefined) {
      if (props.currentUser.id === props.content.autor.id) {
        return (
          <div className={classes.inline}>
            <input
              accept="image/*"
              className={classes.input}
              id="icon-button-file"
              type="file"
            />
            <label htmlFor="icon-button-file">
              <IconButton
                className={classes.button}
                aria-label="upload picture"
                component="span"
              >
                <PhotoCamera />
              </IconButton>
            </label>
            <IconButton
              className={classes.button}
              aria-label="delete argument"
              component="span"
              onClick={() => {
                deleteContent(props);
              }}
            >
              <DeleteForeverIcon />
            </IconButton>
          </div>
        );
      }
    }
  }
}

function displayFactChecking(props, classes, setFactChecking, factChecking) {
  if (props.type === "arg") {
    return (
      <IconButton
        className={classes.button}
        style={{ float: "right" }}
        aria-label="Fackt Checking"
        onClick={() => {
          console.log("Onclick");
          setFactChecking(true);
          console.log(factChecking);
        }}
      >
        <PolicyIcon />
      </IconButton>
    );
  }
}

function updateLikesServer(props, type, argAction) {
  let url = "";
  if (props.type === "arg") {
    if (type === "like") {
      url = UPDATE_LIKE_ARG;
    } else {
      url = UPDATE_DISLIKE_ARG;
    }
  } else if (props.type === "debate") {
    if (type === "like") {
      url = UPDATE_LIKE_DEBATE;
    } else {
      url = UPDATE_DISLIKE_DEBATE;
    }
  }
  url = url + props.content.id + "/" + props.currentUser.id;
  fetch(url, {
    method: "get"
  })
    .then(res => res.json())
    .then(res => {
      if (true) {
        props.updateLikeStore(argAction);
      } else {
        alert("Server Disconnected! Try again later");
      }
    });
}

function getLikeColor(props) {
  if (props.isUserLog === true) {
    return props.type === "arg"
      ? props.currentUser.likesArgs.includes(props.content.id)
        ? "primary"
        : "default"
      : props.currentUser.likesDebate.includes(props.content.id)
      ? "primary"
      : "default";
  }
}

function getDislikeColor(props) {
  if (props.isUserLog === true) {
    return props.type === "arg"
      ? props.currentUser.dislikesArgs.includes(props.content.id)
        ? "secondary"
        : "default"
      : props.currentUser.dislikesDebate.includes(props.content.id)
      ? "secondary"
      : "default";
  }
}

function handleUpdateOfTheStore(props, type) {
  const argAction = {};
  argAction.id = props.content.id;
  argAction.type = props.type;
  argAction.typeLike = type;
  argAction.currentUser = props.currentUser;
  updateLikesServer(props, type, argAction);
  // props.updateLikeStore(argAction);
}

function ConnectedInteractBar(props) {
  const [factChecking, setFactChecking] = useState(false);
  const classes = useStyles();
  return (
    <div style={{ width: "100%" }}>
      <IconButton
        className={classes.likeButton}
        aria-label="like"
        onClick={() => handleUpdateOfTheStore(props, "like")}
        color={getLikeColor(props)}
        disabled={!props.isUserLog}
      >
        <ThumbUpIcon />
      </IconButton>
      {props.content.numberLikes}
      <IconButton
        className={classes.likeButton}
        aria-label="dislike"
        onClick={() => handleUpdateOfTheStore(props, "dislike")}
        color={getDislikeColor(props)}
        disabled={!props.isUserLog}
      >
        <ThumbDownIcon />
      </IconButton>
      {props.content.numberDislikes}
      {displayFactChecking(props, classes, setFactChecking)}
      {displayAutorObjects(props, classes)}
      <FactChecking
        open={factChecking}
        setOpen={setFactChecking}
        content={props.content}
      ></FactChecking>
    </div>
  );
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ConnectedInteractBar);
