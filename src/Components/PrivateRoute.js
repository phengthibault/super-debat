import React, { Component } from "react";
import { Route, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { setisUserLog, setUser } from "../Actions/setUser";
import Navbar from "./navbar";

var axios = require("axios");

const mapStateToProps = state => {
  return { user: state.user, isUserLog: state.isUserLog };
};

function mapDispatchToProps(dispatch) {
  return {
    setUser: user => dispatch(setUser(user))
  };
}

const ProtectedRoute = ({ loggedIn, component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      loggedIn ? <Component {...props} /> : <Redirect to="/" />
    }
  />
);

class PrivateRoute extends Component {
  constructor(props) {
    super(props);
    this.state = {
      log: false
    };
  }
  isUserConnected(loggedIn) {
    console.log(loggedIn);
    let that = this;

    let sessionID = localStorage.getItem("sessionID");
    if (sessionID != null && loggedIn == false) {
      let url = "http://localhost:8082/" + "userConnected/" + sessionID;
      axios.get(url).then(function(response) {
        if (response.data != "") {
          //if we don't get user means session not found in Db
          console.log(response.data);
          that.props.setUser(response.data);
          that.setState({ log: true });
        } else {
          console.log("cookie not found ");
          localStorage.removeItem("sessionID");
        }
      });
      that.setState({ log: false });
    }
  }
  async componentDidMount() {
    await this.isUserConnected(this.props.isUserLog);
  }

  render() {
    console.log(this.props);
    return (
      <div>
        <Navbar />

        <ProtectedRoute
          path={this.props.path}
          loggedIn={this.props.isUserLog}
          component={this.props.component}
        />
      </div>
    );
  }
}
/*
const ProtectedRoute = ({ isUserLog, component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      isUserLog ? <Component {...props} /> : <Redirect to="/Login" />
    }
  />
);*/

export default connect(mapStateToProps, mapDispatchToProps)(PrivateRoute);
