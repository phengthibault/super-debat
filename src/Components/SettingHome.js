import React, { Component } from "react";
import "../CSS/UserProfile.css";

import { connect } from "react-redux";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import { makeStyles } from "@material-ui/core/styles";
import { useState } from "react";
import IconButton from "@material-ui/core/IconButton";
import PhotoCamera from "@material-ui/icons/PhotoCamera";
import MenuItem from "@material-ui/core/MenuItem";
import SaveIcon from "@material-ui/icons/Save";
import { UPDATE_PROFILE_FROM_ID } from "../Constants/urlApi";
import { setUser } from "../Actions/setUser";

const mapStateToProps = state => {
  return { user: state.user, argsFor: state.argsFor };
};

function mapDispatchToProps(dispatch) {
  return {
    setUser: profile => dispatch(setUser(profile))
  };
}

var axios = require("axios");

const useStyles = makeStyles(theme => ({
  paper: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(3)
  },
  submit: {
    margin: theme.spacing(10, 0, 2)
  }
}));

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit" href="/">
        Super Debat
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

function submitOrder(data, props) {
  axios({
    method: "PUT",
    url: UPDATE_PROFILE_FROM_ID + props.user.id + "/" + data.lastPwd,
    data: data
  })
    .then(function(response) {
      console.log(response);
      if (response.data == true) {
        alert("Profile updated!");
        // props.setUser(response);
      } else {
        alert("error", response.data);
      }
    })
    .catch(function(error) {
      console.log(error);
    });
}

function UserProfileDisplayConnected(props) {
  const classes = useStyles();

  const handleInputChange = e => {
    const { name, value } = e.target;
    setValues({ ...values, [name]: value });
  };

  const [values, setValues] = useState({
    surName: props.user.surName,
    lastName: props.user.lastName,
    email: props.user.email,
    imgUrl: props.user.imgUrl,
    login: props.user.login,
    pwd: "",
    lastPwd: "",
    gender: props.user.gender,
    description: props.user.description
  });

  return (
    <div className="row">
      <div className="col-4  pt-3 border-right">
        <div class="Profile">
          <div class="user-profile">
            <img class="avatar" src={props.user.imgUrl} alt="Ash" />
            <div class="username">
              {props.user.lastName} {props.user.surName}
            </div>
            <div class="bio">{props.user.email}</div>
          </div>
        </div>
      </div>
      <div
        className="Setting"
        style={{ marginTop: "3%", position: "relative" }}
      >
        <Container component="main" maxWidth="xs">
          <CssBaseline />
          <div className={classes.paper}>
            <Typography
              component="h1"
              variant="h5"
              style={{ marginBottom: "15px" }}
            >
              Change your parameters
            </Typography>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <TextField
                  autoComplete="fname"
                  name="surName"
                  variant="outlined"
                  required
                  fullWidth
                  id="surName"
                  label="Surname"
                  autoFocus
                  onChange={handleInputChange}
                  value={values.surName}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  variant="outlined"
                  required
                  fullWidth
                  id="lastName"
                  label="Last Name"
                  name="lastName"
                  autoComplete="lname"
                  onChange={handleInputChange}
                  value={values.lastName}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  required
                  fullWidth
                  id="email"
                  label="Email Address"
                  name="email"
                  autoComplete="email"
                  onChange={handleInputChange}
                  value={values.email}
                  type="email"
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  fullWidth
                  disabled
                  id="login"
                  label="login"
                  name="login"
                  autoComplete="Login"
                  onChange={handleInputChange}
                  value={values.login}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  required
                  fullWidth
                  name="lastPwd"
                  label="Last Password"
                  type="lastpassword"
                  id="lastPwd"
                  autoComplete="current-password"
                  onChange={handleInputChange}
                  value={values.lastPwd}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  required
                  fullWidth
                  name="pwd"
                  label="New Password"
                  type="password"
                  id="pwd"
                  autoComplete="current-password"
                  onChange={handleInputChange}
                  value={values.pwd}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  fullWidth
                  name="imgUrl"
                  label="imgUrl"
                  id="imgUrl"
                  autoComplete="Image Url"
                  onChange={handleInputChange}
                  value={values.imgUrl}
                />
                <label htmlFor="icon-button-file">
                  <IconButton
                    color="primary"
                    className={classes.button}
                    aria-label="upload picture"
                    component="span"
                    style={{ float: "right" }}
                  >
                    <PhotoCamera />
                  </IconButton>
                </label>
              </Grid>
            </Grid>
          </div>
          <div className={classes.paper}>
            <Typography
              component="h1"
              variant="h5"
              style={{ marginBottom: "15px" }}
            >
              Tell us a little more about yourself if you want :)
            </Typography>
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <TextField
                  id="gender"
                  select
                  fullWidth
                  label="Select Gender"
                  value={values.gender}
                  onChange={handleInputChange}
                  variant="outlined"
                >
                  <MenuItem key={"male"} value={"male"}>
                    {"Male"}
                  </MenuItem>
                  <MenuItem key={"female"} value={"female"}>
                    {"Female"}
                  </MenuItem>
                  <MenuItem key={"na"} value={"na"}>
                    {"NA"}
                  </MenuItem>
                </TextField>
              </Grid>
              <Grid item xs={12}>
                <TextField
                  autoComplete="fname"
                  name="description"
                  variant="outlined"
                  multiline
                  rows="6"
                  fullWidth
                  id="description"
                  label="Description"
                  onChange={handleInputChange}
                  value={values.description}
                />
              </Grid>
              <Grid item xs={12}>
                <Button
                  variant="contained"
                  startIcon={<SaveIcon />}
                  style={{
                    width: "950",
                    height: "900",
                    float: "right",
                    backgroundColor: "#2196f3"
                  }}
                  onClick={() => submitOrder(values, props)}
                >
                  Save
                </Button>
              </Grid>
            </Grid>
          </div>
          <Box mt={5}>{Copyright()}</Box>
        </Container>
      </div>
    </div>
  );
}
const UserProfileDisplay = connect(
  mapStateToProps,
  mapDispatchToProps
)(UserProfileDisplayConnected);
export default UserProfileDisplay;
