import React, { Component } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Opinion from "./opinion";
import { connect } from "react-redux";
import { FOR, AGAINST, NONE } from "../Constants/utiles";
import IconButton from "@material-ui/core/IconButton";
import CreateIcon from "@material-ui/icons/Create";
import setWritingMode from "../Actions/setWritingMode";
import TextField from "./textField";
import { buttons_color } from "../CSS/color";

function mapStateToProps(state) {
  return {
    argsFor: state.argsFor,
    argsAgainst: state.argsAgainst,
    writingMode: state.writingArg
  };
}
function mapDispatchToProps(dispatch) {
  return {
    setWritingMode: writingMode => dispatch(setWritingMode(writingMode))
  };
}

const useStyle = makeStyles(props => ({
  container: {
    width: "100%",
    position: "relative"
  },
  title: { textAlign: "center", fontWeight: "bold", display: "inline-block" },
  button: {
    position: "absolute",
    right: 0,
    color: buttons_color,
    padding: 0
  }
}));

function loadOpinions(props) {
  switch (props.side) {
    case FOR:
      return props.argsFor.map((arg, index) => {
        return <Opinion key={index} arg={arg}></Opinion>;
      });
    case AGAINST:
      return props.argsAgainst.map((arg, index) => {
        return <Opinion key={index} arg={arg}></Opinion>;
      });
  }
}

function ConnectedOpinions(props) {
  const classes = useStyle(props);
  return (
    <div className={classes.container}>
      <div style={{ textAlign: "center" }}>
        <IconButton
          className={classes.button}
          aria-label="add an argument"
          component="span"
          className={classes.button}
          onClick={() => {
            props.setWritingMode(
              props.writingMode === NONE ? props.side : NONE
            );
          }}
        >
          <CreateIcon />
        </IconButton>
        <div className={classes.title}>{props.side}</div>
      </div>

      {loadOpinions(props)}
      <TextField side={props.side}></TextField>
    </div>
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(ConnectedOpinions);
