import Autosuggest from "react-autosuggest";
import React, { Component } from "react";
import { connect } from "react-redux";
import "../CSS/SearchBar.css";
import "axios";
import {
  GET_KEYWORDS_SEARCHBAR,
  GET_NEW_DEBATES_SEARCHBAR,
  GET_TOP_DEBATES
} from "../Constants/urlApi";
import setDebates from "../Actions/setDebates";

const axios = require("axios");
function mapDispatchToProps(dispatch) {
  return {
    setDebates: debates => dispatch(setDebates(debates))
  };
}
// Imagine you have a list of lDebatTitle that you'd like to autosuggest.
/*const lDebatTitle = [
  {
    name: "C",
    year: 1972
  },
  {
    name: "Elm",
    year: 2012
  }
];*/
//var lDebatTitle = ["test", "retirement"];
/*
function getDebatsTitle() {
  axios
    .get("http://localhost:8082/Debats/Title")
    .then(function(response) {
      lDebatTitle = response.data;
    })
    .catch(function(error) {
      console.log(error);
    });
}*/

class SearchBar extends React.Component {
  constructor(props) {
    super(props);

    // Autosuggest is a controlled component.
    // This means that you need to provide an input value
    // and an onChange handler that updates this value (see below).
    // Suggestions also need to be provided to the Autosuggest,
    // and they are initially empty because the Autosuggest is closed.
    this.state = {
      value: "",
      suggestions: [],
      lDebatTitle: ["test", "retirement"]
    };
  }

  componentDidMount() {
    try {
      fetch(GET_KEYWORDS_SEARCHBAR)
        .then(response => response.json())
        .then(keywords => this.setState({ lDebatTitle: keywords }));
    } catch (error) {
      console.error(error);
    }
  }

  onChange = (event, { newValue }) => {
    this.setState({
      value: newValue
    });
  };

  onKeyDown = e => {
    if (e.keyCode == 13) {
      console.log(this.state);
      if (this.state.lDebatTitle.includes(this.state.value)) {
        fetch(GET_NEW_DEBATES_SEARCHBAR + "/" + this.state.value, {
          method: "post"
        })
          .then(response => {
            return response.json();
          })
          .then(debates => {
            this.props.setDebates(debates);
          });
      } else if (this.state.value == "") {
        fetch(GET_TOP_DEBATES)
          .then(response => {
            return response.json();
          })
          .then(debates => {
            this.props.setDebates(debates);
          });
      }
    }
  };

  // Teach Autosuggest how to calculate suggestions for any given input value.
  getSuggestions = value => {
    const inputValue = value.trim().toLowerCase();
    const inputLength = inputValue.length;

    return inputLength === 0
      ? []
      : this.state.lDebatTitle.filter(
          title => title.toLowerCase().slice(0, inputLength) === inputValue
        );
  };

  // When suggestion is clicked, Autosuggest needs to populate the input
  // based on the clicked suggestion. Teach Autosuggest how to calculate the
  // input value for every given suggestion.
  //const getSuggestionValue = suggestion => suggestion; //suggestion.name

  getSuggestionValue(suggestion) {
    console.log(suggestion);
    return suggestion;
  }
  // Use your imagination to render suggestions.
  renderSuggestion = suggestion => (
    <div style={{ color: "red" }}>{suggestion}</div>
  ); //suggestion.name

  // Autosuggest will call this function every time you need to update suggestions.
  // You already implemented this logic above, so just use it.
  onSuggestionsFetchRequested = ({ value }) => {
    this.setState({
      suggestions: this.getSuggestions(value)
    });
  };

  // Autosuggest will call this function every time you need to clear suggestions.
  onSuggestionsClearRequested = () => {
    this.setState({
      suggestions: []
    });
  };

  render() {
    const { value, suggestions } = this.state;

    // Autosuggest will pass through all these props to the input.
    const inputProps = {
      placeholder: "Search...",
      value,
      onChange: this.onChange,
      onKeyDown: this.onKeyDown
    };

    // Finally, render it!
    return (
      <Autosuggest
        suggestions={suggestions}
        onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
        onSuggestionsClearRequested={this.onSuggestionsClearRequested}
        getSuggestionValue={this.getSuggestionValue}
        renderSuggestion={this.renderSuggestion}
        inputProps={inputProps}
      />
    );
  }
}

export default connect(null, mapDispatchToProps)(SearchBar);
