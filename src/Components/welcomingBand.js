import React, { Component } from "react";
import { connect } from "react-redux";
import Button from "@material-ui/core/Button";
import { BrowserRouter as Router, Link } from "react-router-dom";
import { buttons_color } from "../CSS/color";
const mapStateToProps = state => {
  return {
    user: state.user,
    argsFor: state.argsFor,
    isUserLog: state.isUserLog
  };
};

class WelcomingBand extends Component {
  constructor(props) {
    super(props);
    this.state = {
      displayButtonCreate: "none"
    };
    if (this.props.isUserLog == true) {
      this.state.displayButtonCreate = "";
    }
  }

  render() {
    return (
      <div>
        <div style={{ width: "100%", height: "50px" }}></div>
        <div
          style={{
            width: "100%",
            textAlign: "center",
            fontSize: "large",
            fontWeight: "bold",
            color: "black"
          }}
        >
          Welcome to the best debate website! Here you can find and participate
          to all the debates that you want !!!
          <br /> First, you can pick an existing debate or search for one.
          <br />
          Then, feel free to participate ;) !!
        </div>
        <div style={{ textAlign: "center", margin: "4vh" }}>
          <Button
            variant="outlined"
            color="secondary"
            style={{
              display: this.state.displayButtonCreate,
              color: buttons_color,
              border: "1px solid rgba(0, 0, 0, 0.23)"
            }}
            component={Link}
            to={"/Create"}
          >
            Create Debat
          </Button>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps)(WelcomingBand);
