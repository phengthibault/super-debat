import React, { Component, useState } from "react";
import TextField from "@material-ui/core/TextField";
import { connect } from "react-redux";
import { NONE } from "../Constants/utiles";
import Button from "@material-ui/core/Button";
import setWritingMode from "../Actions/setWritingMode";
import { CHECK_ARGUMENT } from "../Constants/urlApi";
import { Text } from "react";
import { POST_ARGUMENT } from "../Constants/urlApi";
import { withRouter } from "react-router-dom";
import { compose } from "redux";
import {
  GET_DEBATE_FROM_ID,
  GET_ARGSAGAINST_FROM_DEBATE_ID,
  GET_ARGSFOR_FROM_DEBATE_ID
} from "../Constants/urlApi";
import { setArgsPour, setArgsContre } from "../Actions/setArgs";
import logo from "../Images/Waiting.gif";
import CloudDoneIcon from "@material-ui/icons/CloudDone";
import { setisUserLog } from "../Actions/setUser";
import { buttons_color } from "../CSS/color";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { makeStyles } from "@material-ui/core/styles";
import { for_color, against_color } from "../CSS/color";
var axios = require("axios");

function mapStateToProps(state) {
  return {
    isUserLog: state.isUserLog,
    writingMode: state.writingArg,
    user: state.user,
    selectedDebate: state.selectedDebate
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setWritingMode: writingMode => dispatch(setWritingMode(writingMode)),
    setArgsPour: argsFor => dispatch(setArgsPour(argsFor)),
    setArgsContre: argsAgainst => dispatch(setArgsContre(argsAgainst)),
    setisUserLog: isUserLog => dispatch(setisUserLog(isUserLog))
  };
}

const useStyles = makeStyles(theme => ({
  container: { backgroundColor: "black" }
}));

function TextFields(props) {
  const [input, setInput] = useState("");
  const [output, setOutput] = useState([]);

  const [misspelledWords, setMisspelledWords] = useState([]);
  const [misspelledWordsStr, setMisspelledWordsStr] = useState([]);

  const [profaneWords, setProfaneWords] = useState([]);

  const [keyWords, setKeyWords] = useState({});
  const [keyWordsStr, setKeyWordsStr] = useState([]);
  var test;
  const [similarArgs, setSimilarArgs] = useState([]);

  const [requestSent, setRequestSent] = useState(false);
  const [ischecked, setIschecked] = useState(false);
  const [correctionReceived, setCorrectionReceived] = useState(false);

  const classes = useStyles();
  // ---------- SENDING TO PYTHON BACKEND --------------------

  function checkArgumentPython(props, input) {
    setRequestSent(true);
    setCorrectionReceived(false);
    axios
      .post("http://127.0.0.1:5000/correct_comment", {
        user_input: input,
        debate: props.selectedDebate.id
      })
      .then(function(response) {
        const dict = response["data"];
        console.log(dict);

        setOutput(dict["finalText"]);

        setMisspelledWords(dict["misspelledWords"]);
        setMisspelledWordsStr(dict["misspelledWordsStr"]);

        setProfaneWords(dict["profaneWords"]);

        setKeyWords(dict.keyWords);
        console.log(keyWords);

        setKeyWordsStr(dict["keyWordsStr"]);
        setSimilarArgs(dict["similarArgs"]);

        doubleTry(dict);

        setIschecked(dict["argumentValid?"]);

        setCorrectionReceived(true);
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  function doubleTry(dict) {
    setMisspelledWords(dict["misspelledWords"]);
    setKeyWords(dict.keyWords);
    console.log(keyWords);
    setSimilarArgs(dict["similarArgs"]);
  }

  function loadExplanationText() {
    return requestSent === true ? (
      <div style={{ marginTop: "1%" }}>
        <TextField
          label="Explanation"
          id="outlined-multiline-static"
          fullWidth={true}
          value="Your text is analysed. Words in yellow are misspelled, words in red will be blocked. Please correct if needed."
          multiline
          rows="2"
          variant="outlined"
          style={{ backgroundColor: "white" }}
        />
        {loadWaitingGif()}
      </div>
    ) : (
      <span></span>
    );
  }

  function loadWaitingGif() {
    if (correctionReceived === false) {
      return (
        <div style={{ marginTop: "1%", marginTop: "1%", textAlign: "center" }}>
          <img src={logo} alt="loading..." />
        </div>
      );
    }
  }

  function loadList(List) {
    return List.map(value => {
      return <li>{value}</li>;
    });
  }

  function loadListKeyWords(keyWords) {
    return keyWords.map(value => {
      return <li>{value}</li>;
    });
  }

  function loadValidationButton(input, props, setInput, keyWords) {
    return ischecked === true ? (
      <Button
        variant="outlined"
        color={buttons_color}
        style={{
          float: "right",
          marginTop: "1%",
          marginLeft: "1%",
          color: "primary",
          border: "1px solid rgba(0, 0, 0, 0.23)"
        }}
        onClick={() => postArg(input, props, setInput, keyWords)}
      >
        Send your argument
      </Button>
    ) : (
      <span></span>
    );
  }

  function loadCorrectedText(output) {
    return output.map(value => {
      if (misspelledWords.includes(value)) {
        return <span style={{ background: "yellow" }}>{value}&nbsp;</span>;
      } else if (profaneWords.includes(value)) {
        return <span style={{ background: "red" }}>{value}&nbsp;</span>;
      } else {
        return <span style={{ background: "#FFFAF0" }}>{value}&nbsp;</span>;
      }
    });
  }

  function loadIfErrors() {
    return misspelledWordsStr.length != 0 || profaneWords.length != 0 ? (
      <div>
        <div>
          <h3>Corrected text : </h3>
          {loadCorrectedText(output)}
        </div>
        <div id="corrections block">
          <h3>Correction attempts : </h3>
          <ul>{loadList(misspelledWordsStr)}</ul>
        </div>
      </div>
    ) : (
      <div style={{ textAlign: "center" }}>
        <h3>No errors found, your argument is valid.</h3>
      </div>
    );
  }

  function loadIfChecked() {
    if (ischecked === true) {
      return (
        <div>
          <div id="corrections block">
            <h3>Keywords found : </h3>
            <ul>{loadListKeyWords(keyWordsStr)}</ul>
          </div>
          <div id="similarities block">
            <h3>Similar arguments : </h3>
            <ul>{loadList(similarArgs)}</ul>
          </div>
        </div>
      );
    }
  }

  function loadCorrectiondiv() {
    return correctionReceived === true ? (
      <div
        id="correction block"
        style={{
          backgroundColor: "#FFFAF0",
          borderStyle: "solid",
          marginTop: "1%"
        }}
      >
        <div style={{ textAlign: "center" }}>
          <h2>Analyse results</h2>
        </div>
        {loadIfErrors()}
        {loadIfChecked()}
      </div>
    ) : (
      <div></div>
    );
  }

  // ---------- SENDING TO JAVA BACKEND --------------------

  function postArg(content2, props, setInput, keyWords2) {
    let inFavor2 = props.side == "Agree" ? true : false;
    let idDebat2 = props.selectedDebate.id;
    let url;
    if (props.isUserLog != false) {
      url = POST_ARGUMENT + "/" + props.user.id;
    } else {
      url = POST_ARGUMENT;
    }

    console.log(content2);

    axios
      .post(url, {
        content: content2,
        inFavor: inFavor2,
        idDebat: idDebat2,
        keyWords: keyWords2
      })
      .then(function(response) {
        console.log(response);
        if (response.data == true) {
          //  props.history.push("/debate/" + idDebat2);
          setArgs(idDebat2, props);
          setInput("");
          props.setWritingMode(NONE);
        }
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  function setArgs(id, props) {
    fetch(GET_ARGSAGAINST_FROM_DEBATE_ID + id)
      .then(response => {
        return response.json();
      })
      .then(argsAgainst => {
        props.setArgsContre(argsAgainst);
      });
    fetch(GET_ARGSFOR_FROM_DEBATE_ID + id)
      .then(response => {
        return response.json();
      })
      .then(argsFor => {
        props.setArgsPour(argsFor);
      });
  }
  console.log(props.writingMode);

  return (
    <div>
      <Dialog
        open={props.writingMode !== NONE && props.side == props.writingMode}
        onClose={() => {}}
        aria-labelledby="form-dialog-title"
        fullWidth={true}
        maxWidth={"xl"}
      >
        <DialogTitle id="form-dialog-title" style={{ textAlign: "center" }}>
          Add an argument
        </DialogTitle>
        <DialogContent>
          <div
            style={{
              backgroundColor:
                props.side == "Agree" ? for_color : against_color,
              padding: "10px",
              borderRadius: "10px"
            }}
          >
            <TextField
              label="Write your comment here!"
              id="outlined-multiline-static"
              fullWidth={true}
              value={input}
              onChange={e => setInput(e.target.value)}
              multiline
              rows="2"
              variant="outlined"
              style={{ backgroundColor: "white" }}
            />
            {loadExplanationText()}
            {loadCorrectiondiv()}
          </div>
          <Button
            style={{
              marginTop: "1%",
              color: buttons_color,
              border: "1px solid rgba(0, 0, 0, 0.23)"
            }}
            variant="outlined"
            color="secondary"
            onClick={() => {
              console.log("Onclick");
              props.setWritingMode(NONE);
            }}
          >
            <ExitToAppIcon
              style={{ transform: "rotate(180deg)" }}
            ></ExitToAppIcon>
          </Button>
          {loadValidationButton(input, props, setInput)}
          <Button
            variant="outlined"
            color="primary"
            style={{
              float: "right",
              marginTop: "1%",
              color: buttons_color,
              border: "1px solid rgba(0, 0, 0, 0.23)"
            }}
            onClick={() => {
              checkArgumentPython(props, input);
            }}
          >
            <CloudDoneIcon></CloudDoneIcon>
          </Button>
        </DialogContent>
      </Dialog>
    </div>
  );
}

//export default connect(mapStateToProps, mapDispatchToProps)withRouter(TextFields);
export default compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps)
)(TextFields);
