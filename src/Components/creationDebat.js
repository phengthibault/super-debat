import React, { Component } from "react";
import "../CSS/UserProfile.css";
import { connect } from "react-redux";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import PhotoCamera from "@material-ui/icons/PhotoCamera";
import { Link } from "react-router-dom";
import TextField from "@material-ui/core/TextField";
import { func } from "prop-types";
import { POST_DEBATE } from "../Constants/urlApi";
import { withRouter } from "react-router-dom";
import { buttons_color } from "../CSS/color";
var axios = require("axios");

const mapStateToProps = state => {
  return { user: state.user, argsFor: state.argsFor };
};

const styles = {
  truncate: {
    width: 500,
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis"
  }
};

const toolbarStyle = {
  display: "flex",
  width: "300px",
  backgroundColor: "#d6e2ea",
  padding: "6px 8px"
};

function fonction_m(val) {
  return val;
}

function postDebat(dataForm, props) {
  console.log(props);
  axios
    .post(POST_DEBATE, {
      title: dataForm.title,
      autor: props.user,
      imgUrl: dataForm.imgUrl,
      describe: dataForm.describe
    })
    .then(function(response) {
      console.log(response);
      if (response.data == true) {
        alert("your debate has been created");
        props.history.push("/");
      }
    })
    .catch(function(error) {
      console.log(error);
    });
}

function checkDebatPython(dataForm, props) {
  //setFlagPost(true);
  axios
    .post("http://127.0.0.1:5000/correct_debat", {
      content: dataForm.describe
    })
    .then(function(response) {
      console.log(response);

      /*setOutput(python_response["data"]["final_text"]);
      setSpellChecked(python_response["data"]["list_correction_words"]);

      setMisspelled(python_response["data"]["misspelled_words"]);
      setProfaneWords(python_response["data"]["profane_words"]);

      doubleTry(python_response["data"]["list_correction_words"]);

      setFlag(true);
      setIschecked(python_response["data"]["validate_argument"]);*/
    })
    .catch(function(error) {
      console.log(error);
    });
}

class CreationDebat extends Component {
  constructor(props) {
    super(props);
    this.state = {
      //editorState: EditorState.createEmpty()
    };
  }

  state = {
    title: "",
    autor: "",
    imgUrl: "",
    describe: ""
  };

  render() {
    return (
      <div>
        <Typography
          variant="h6"
          gutterBottom
          style={{ textAlign: "center", fontWeight: "bold", marginTop: "30px" }}
        >
          Create your debate
        </Typography>

        <Grid container spacing={5}>
          <Grid
            container
            justify="center"
            item
            xs={10}
            sm={10}
            style={{
              //width: "6rem",
              marginTop: "3%",
              //marginRight: "10%",
              marginLeft: "5%"
              //marginBottom: "50%"
            }}
          >
            <TextField
              style={{ width: "50rem" }}
              autoComplete="fname"
              name="title"
              variant="outlined"
              required
              fullWidth
              id="Title"
              label="Title"
              autoFocus
              onChange={e => this.setState({ title: e.target.value })}
              value={this.state.title}
            />
          </Grid>

          <Grid
            container
            justify="center"
            item
            xs={20}
            sm={10}
            style={{
              //width: "35rem",
              // height:"20rem",
              marginTop: "3%",
              //marginRight: "10%",
              marginLeft: "5%"
              //marginBottom: "50%"
            }}
          >
            <TextField
              style={{ width: "50rem" }}
              id="outlined-multiline-static"
              label="Content"
              multiline
              rows="4"
              //defaultValue="Content"
              variant="outlined"
              value={this.state.describe}
              onChange={e => this.setState({ describe: e.target.value })}
            ></TextField>
          </Grid>

          <Grid
            container
            justify="center"
            item
            xs={10}
            sm={10}
            style={{
              //width: "6rem",
              marginTop: "3%",
              //marginRight: "10%",
              marginLeft: "5%"
              //marginBottom: "50%"
            }}
          >
            <TextField
              variant="outlined"
              fullWidth
              name="imgUrl"
              label="imgUrl"
              id="imgUrl"
              autoComplete="Image Url"
              style={{ width: "50rem" }}
              value={this.state.imgUrl}
              onChange={e => this.setState({ imgUrl: e.target.value })}
            />
          </Grid>
        </Grid>

        <Grid container justify="center" style={{ marginTop: "1%" }}>
          <Typography container justify="center" variant="h6" gutterBottom>
            Attach picture with your debate
          </Typography>

          <label htmlFor="icon-button-file">
            <PhotoCamera />
          </label>
        </Grid>
        {console.log(fonction_m(this.state.myInputValue))}
        <Grid style={{ marginTop: "2%", marginLeft: "45%" }}>
          <Button
            variant="outlined"
            color="secondary"
            onClick={this.changeView}
            style={{
              marginBottom: "1%",
              marginLeft: "10%",
              marginRight: "1%",
              color: buttons_color,
              border: "1px solid rgba(0, 0, 0, 0.23)"
            }}
          >
            <Link style={{ textDecoration: "none", color: "black" }} to="/">
              Return
            </Link>
          </Button>
          <Button
            variant="outlined"
            color="primary"
            style={{
              marginBottom: "1%",
              marginLeft: "0%",
              color: buttons_color,
              border: "1px solid rgba(0, 0, 0, 0.23)"
            }}
            onClick={() => postDebat(this.state, this.props)}
          >
            Create My Debate
          </Button>
        </Grid>
      </div>
    );
  }
}

export default withRouter(connect(mapStateToProps)(CreationDebat));
