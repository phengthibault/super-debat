import React, { Component } from "react";
import { connect } from "react-redux";
import DebateCardView from "./debateCardView";
import setDebates from "../Actions/setDebates";
import GridListHome from "./gridListHome";
import { GET_TOP_DEBATES } from "../Constants/urlApi";
import Grid from "@material-ui/core/Grid";

function mapStateToProps(state) {
  return { debates: state.debates, topArgs: state.topArgs, users: state.users };
}

function mapDispatchToProps(dispatch) {
  return {
    setDebates: debates => dispatch(setDebates(debates))
  };
}

class containerTopDebats extends Component {
  getTopDebates() {
    fetch(GET_TOP_DEBATES)
      .then(response => {
        return response.json();
      })
      .then(myJson => {
        this.props.setDebates(myJson);
      });
  }
  componentDidMount() {
    this.getTopDebates();
  }
  displayTopDebates() {
    if (this.props.debates.length === 0) {
      this.getTopDebates();
    }
    return this.props.debates.map(debat => {
      return (
        <Grid item xs={4}>
          <DebateCardView
            debate={debat}
            topArg={debat.topArg}
            autorDebate={debat.autor}
            autorTopArg={debat.topArg.autor}
          ></DebateCardView>
        </Grid>
      );
    });
  }

  render() {
    return (
      <div>
        <Grid container spacing={2} direction="row" alignItems="stretch">
          {this.displayTopDebates()}
        </Grid>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(containerTopDebats);
