import React, { Component } from "react";
import { connect } from "react-redux";
import { fade, makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import InputBase from "@material-ui/core/InputBase";
import SearchIcon from "@material-ui/icons/Search";
import logo from "../Images/logo.png";
import ProfileIcon from "./profileIcon";
import SearchBar from "./SearchBar";
import { BrowserRouter as Router, Link } from "react-router-dom";
import { navbar_color } from "../CSS/color";
function mapStateToProps(state) {
  return { isUserLog: state.isUserLog, user: state.user, color: state.color };
}

const useStyles = makeStyles(theme => ({
  navbar: { backgroundColor: navbar_color },
  search: {
    position: "relative",
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.8),
    "&:hover": {
      backgroundColor: fade(theme.palette.common.white, 0.8)
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing(3),
      width: "auto"
    }
  },
  sectionDesktop: {
    display: "none",
    [theme.breakpoints.up("md")]: {
      display: "flex"
    }
  },
  searchIcon: {
    width: theme.spacing(7),
    height: "100%",
    position: "absolute",
    pointerEvents: "none",
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  inputRoot: {
    color: "inherit"
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 7),
    transition: theme.transitions.create("width"),
    width: "30%",
    [theme.breakpoints.up("md")]: {
      width: 200
    },
    profileIcon: { float: "right" }
  }
}));

function PrimarySearchAppBar(props) {
  const classes = useStyles();
  return (
    <AppBar position="sticky" className={classes.navbar}>
      <Toolbar>
        <div style={{ display: "flex", width: "100%" }}>
          <div
            style={{
              flexGrow: "1",
              display: "flex",
              justifyContent: "center",
              flexDirection: "column"
            }}
          >
            <div className={classes.search} style={{ width: "fit-content" }}>
              <SearchBar
                classes={{
                  root: classes.inputRoot,
                  input: classes.inputInput
                }}
              />
            </div>
          </div>
          <div style={{ flexGrow: "1" }}>
            <Link to={"/"}>
              <img src={logo} alt="logo" width="75" height="75" />
            </Link>
          </div>
          <div
            style={{
              flexGrow: "1",
              display: "flex",
              justifyContent: "center",
              flexDirection: "column"
            }}
          >
            <ProfileIcon
              className={classes.profileIcon}
              user={props.user}
              side="right"
              currentUserMode={true}
            ></ProfileIcon>
          </div>
        </div>
      </Toolbar>
    </AppBar>
  );
}

export default connect(mapStateToProps)(PrimarySearchAppBar);
