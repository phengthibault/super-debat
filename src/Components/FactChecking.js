import React, { useState } from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Grid from "@material-ui/core/Grid";
import ProfileIcon from "./profileIcon";
import { connect } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import IconButton from "@material-ui/core/IconButton";
import CreateIcon from "@material-ui/icons/Create";
import Slide from "@material-ui/core/Slide";
import FormLabel from "@material-ui/core/FormLabel";
import FormControl from "@material-ui/core/FormControl";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import { SET_FACT_CHECKING } from "../Constants/urlApi";
import setFactChecking from "../Actions/setFactChecking";
import { for_color, against_color } from "../CSS/color";
function mapStateToProps(state) {
  return {
    argsFor: state.argsFor,
    argsAgainst: state.argsAgainst,
    user: state.user,
    isUserLog: state.isUserLog
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setFactChecking: arg => dispatch(setFactChecking(arg))
  };
}

const useStyles = makeStyles(theme => ({
  profileIcon: { position: "absolute", top: 0, left: 0 },
  container: {
    margin: "5px",
    padding: "5px",
    border: "2px solid black",
    borderRadius: "5px",
    position: "relative",
    backgroundColor: "white"
  },
  title: { textAlign: "center", marginBottom: "22px", fontWeight: "bold" },
  content: { margin: "5px 0px" },
  informativeContent: { fontWeight: "bold" },
  pen: { position: "absolute", top: 0, right: 0 },
  button: { display: "inline-block", position: "relative", float: "right" },
  dialog: { position: "relative" },
  textFields: { display: "block" }
}));

function buildFactCheckingPanel(factCheckings, classes) {
  if (factCheckings !== undefined) {
    return factCheckings.map(factChecking => {
      console.log(factChecking);
      let content = [
        <div className={classes.title}>{factChecking.title}</div>,
        <div>
          <div className={classes.content}>
            <span className={classes.informativeContent}>Describe : </span>
            {factChecking.content}
          </div>
          <div className={classes.content}>
            <span className={classes.informativeContent}>Link : </span>
            {factChecking.url}
          </div>
        </div>
      ];
      if (factChecking.autor != null) {
        return (
          <div className={classes.container}>
            <ProfileIcon
              user={factChecking.autor}
              className={classes.profileIcon}
            ></ProfileIcon>
            {content}
          </div>
        );
      } else {
        return <div className={classes.container}>{content}</div>;
      }
    });
  }
}

function FactChecking(props) {
  const classes = useStyles(props);
  const [write, setWrite] = useState(false);
  const [factsChecking, setFactsChecking] = useState(true);
  const [side, setSide] = useState(null);
  const [title, setTitle] = useState(null);
  const [url, setUrl] = useState(null);
  const [content, setContent] = useState(null);

  const handleClickOpen = () => {
    props.setOpen(true);
  };

  const handleClose = () => {
    props.setOpen(false);
  };

  const sendFactChecking = () => {
    let factChecking = {};
    if (title != null && side !== null && url !== null && content !== null) {
      factChecking.id = props.content.id;
      factChecking.side = side;
      factChecking.url = url;
      factChecking.content = content;
      props.isUserLog
        ? (factChecking.autor = {
            lastName: props.user.lastName,
            surName: props.user.surName,
            id: props.user.id
          })
        : (factChecking.autor = null);
    }
    let url2 = SET_FACT_CHECKING + factChecking.id + "/" + factChecking.side;
    fetch(url2, {
      method: "POST", // or 'PUT'
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(factChecking)
    })
      .then(response => response.json())
      .then(data => {
        if (true) {
          factChecking.argsAgainst = props.argsAgainst;
          factChecking.argsFor = props.argsFor;
          props.setFactChecking(factChecking);
          setFactChecking(true);
          alert("factcheckadd");
        }
      })
      .catch(error => {
        console.error("Error:", error);
      });
  };

  return (
    <div>
      <Dialog
        open={props.open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
        fullWidth={true}
        maxWidth={"xl"}
        className={classes.dialog}
      >
        <DialogTitle id="form-dialog-title" style={{ textAlign: "center" }}>
          Fact Checkings Links
        </DialogTitle>
        <div className={classes.pen}>
          <IconButton
            color="primary"
            aria-label="add a fact checkingt"
            component="div"
            className={classes.button}
            onClick={() => {
              setFactsChecking(!factsChecking);
              if (write) {
                setWrite(false);
              }
            }}
          >
            <CreateIcon />
          </IconButton>
        </div>
        <Slide
          direction="right"
          in={factsChecking}
          appear={false}
          unmountOnExit={true}
          onExited={() => {
            setWrite(!write);
          }}
          timeout={500}
        >
          <DialogContent>
            <DialogContentText style={{ textAlign: "center" }}>
              Here are displayed links that will validate or devalidate this
              argument.
            </DialogContentText>

            <Grid container margin={1}>
              <Grid item xs={6}>
                <div
                  style={{
                    margin: "8px",
                    padding: "4px",
                    backgroundColor: for_color,
                    borderRadius: "10px"
                  }}
                >
                  <div style={{ textAlign: "center", fontWeight: "bold" }}>
                    Agree
                  </div>
                  {props.argsFor.map(arg => {
                    if (props.content.id === arg.id) {
                      console.log(props.content);
                      return buildFactCheckingPanel(
                        arg.factCheckings.For,
                        classes
                      );
                    }
                  })}
                  {props.argsAgainst.map(arg => {
                    if (props.content.id === arg.id) {
                      console.log(props.content);
                      return buildFactCheckingPanel(
                        arg.factCheckings.For,
                        classes
                      );
                    }
                  })}
                </div>
              </Grid>
              <Grid item xs={6}>
                <div
                  style={{
                    margin: "8px",
                    padding: "4px",
                    backgroundColor: against_color,
                    borderRadius: "10px"
                  }}
                >
                  <div style={{ textAlign: "center", fontWeight: "bold" }}>
                    Disagree
                  </div>
                  {props.argsFor.map(arg => {
                    if (props.content.id === arg.id) {
                      console.log(props.content);
                      return buildFactCheckingPanel(
                        arg.factCheckings.Against,
                        classes
                      );
                    }
                  })}
                  {props.argsAgainst.map(arg => {
                    if (props.content.id === arg.id) {
                      console.log(props.content);
                      return buildFactCheckingPanel(
                        arg.factCheckings.Against,
                        classes
                      );
                    }
                  })}
                </div>
              </Grid>
            </Grid>
          </DialogContent>
        </Slide>

        <div style={{ margin: "auto", display: write ? "block" : "none" }}>
          <DialogContent>
            <DialogContentText>
              Add an URL if you want to fact-checked this argument.
            </DialogContentText>
          </DialogContent>
          <FormControl
            component="fieldset"
            required
            className={classes.formControl}
            fullWidth
            style={{ textAlign: "center" }}
          >
            <FormLabel component="legend">Choose your side </FormLabel>
            <FormGroup
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                margin: "8px"
              }}
            >
              <FormControlLabel
                control={
                  <Checkbox
                    checked={side === "for"}
                    value="for"
                    onChange={() => setSide("for")}
                  />
                }
                label="For"
              />
              <FormControlLabel
                control={
                  <Checkbox
                    checked={side === "against"}
                    value="against"
                    onChange={() => setSide("against")}
                  />
                }
                label="Against"
              />
            </FormGroup>
          </FormControl>

          <TextField
            className={classes.textFields}
            name="title"
            fullWidth
            variant="outlined"
            required
            margin="normal"
            id="title"
            label="Title"
            autoFocus
            onChange={event => {
              setTitle(event.target.value);
            }}
          />
          <TextField
            className={classes.textFields}
            name="url"
            fullWidth
            variant="outlined"
            required
            margin="normal"
            id="url"
            label="Url"
            autoFocus
            onChange={event => {
              setUrl(event.target.value);
            }}
          />
          <TextField
            className={classes.textFields}
            name="describe"
            variant="outlined"
            required
            id="describe"
            label="Describe"
            autoFocus
            fullWidth
            margin="normal"
            multiline
            rows="4"
            onChange={event => {
              setContent(event.target.value);
            }}
          />
          <Button
            onClick={sendFactChecking}
            color="primary"
            variant="outlined"
            style={{ float: "right" }}
          >
            Send your argument
          </Button>
        </div>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(FactChecking);
