import React, { Component } from "react";
import "../CSS/UserProfile.css";
import { connect } from "react-redux";
import Opinion from "./opinion";
import Navbar from "./navbar";
import setProfile from "../Actions/setProfile";
import { GET_USER_FROM_ID, GET_COMMENT_USER } from "../Constants/urlApi";
import setArgsUser from "../Actions/setArgsUser";
const mapStateToProps = state => {
  return {
    user: state.user,
    argsFor: state.argsFor,
    profile: state.profile,
    args: state.args
  };
};

function mapDispatchToProps(dispatch) {
  return {
    setProfile: profile => dispatch(setProfile(profile)),
    setArgsUser: args => dispatch(setArgsUser(args))
  };
}

function getmyComments(props) {
  let url = GET_COMMENT_USER + props.user.id;
  fetch(url)
    .then(response => response.json())
    .then(data => {
      props.setArgsUser(data);
    });
  if (props.args != null) {
    return props.args.map((arg, i) => <Opinion arg={arg} />);
  }
}

class UserProfileDisplayConnected extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  getUserInformations(id) {
    let that = this;
    fetch(GET_USER_FROM_ID + id)
      .then(function(response) {
        return response.json();
      })
      .then(function(user) {
        console.log(user);
        that.props.setProfile(user);
      });
  }

  componentDidMount() {
    const id = this.props.match.params.id; //this.props.match.params.id; TODO Remplacer l'id pour le prendre de l'url
    console.log(id);
    if (this.props.user.id == id) {
      this.props.setProfile(this.props.user);
    } else {
      this.getUserInformations(id);
    }
  }

  render() {
    return (
      <div>
        <Navbar></Navbar>
        <div className="row">
          <div className="col-4  pt-3 border-right">
            <div class="Profile">
              <div class="user-profile">
                <img
                  class="avatar"
                  src={this.props.profile.imgUrl}
                  alt="Photo"
                />
                <div class="username">
                  {this.props.user.lastName} {this.props.profile.surName}
                </div>
                <div class="bio">{this.props.profile.email}</div>
                <div class="description"></div>
              </div>
            </div>
          </div>
          <div className="col-10  pt-3 bg-white" style={{ marginTop: "%" }}>
            {/* Comment List Component */}

            <div style={{ width: "50%", margin: "auto" }}>
              <div style={{ textAlign: "center" }}>
                <h4>My comments</h4>
              </div>

              {getmyComments(this.props)}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const UserProfileDisplay = connect(
  mapStateToProps,
  mapDispatchToProps
)(UserProfileDisplayConnected);
export default UserProfileDisplay;
