import React, { Component } from "react";
import Typography from "@material-ui/core/Typography";
import ProfileIcon from "./profileIcon";

class TopComment extends Component {
  displayTopArg = () => {
    if (this.props.topArg === undefined) {
      return <div></div>;
    } else {
      return (
        <div style={{ "border-radius": "30px", border: "solid 2px" }}>
          <Typography paragraph align="center">
            <ProfileIcon user={this.props.topArg.autor}></ProfileIcon>
            <span style={{ "font-weight": "bold" }}>
              Top Comment :{" "}
              <span style={{ color: this.props.topArg.side ? "green" : "red" }}>
                {this.props.topArg.side ? "Agrees" : "Disagrees"}
              </span>
            </span>
          </Typography>
          <Typography paragraph>{this.props.topArg.content}</Typography>
        </div>
      );
    }
  };
  render() {
    return this.displayTopArg();
  }
}

export default TopComment;
