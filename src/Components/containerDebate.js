import React, { Component } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Opinions from "./opinions";
import Button from "@material-ui/core/Button";
import WhatshotIcon from "@material-ui/icons/Whatshot";
import AcUnitIcon from "@material-ui/icons/AcUnit";
import { FOR, AGAINST, NONE } from "../Constants/utiles";
import { connect } from "react-redux";
import { setArgsPour, setArgsContre } from "../Actions/setArgs";
import {
  GET_SORTED_ARGSFOR_HOT,
  GET_SORTED_ARGSAGAINST_HOT,
  GET_SORTED_ARGSAGAINST_FRESH,
  GET_SORTED_ARGSFOR_FRESH
} from "../Constants/urlApi";
import {
  body_color,
  for_color,
  against_color,
  buttons_color
} from "../CSS/color";
function mapStateToProps(state) {
  return { selectedDebate: state.selectedDebate, writingArg: state.writingArg };
}

function mapDispatchToProps(dispatch) {
  return {
    setArgsPour: args => dispatch(setArgsPour(args)),
    setArgsContre: args => dispatch(setArgsContre(args))
  };
}

function getSortedArgs(urlfor, urlagainst, props) {
  fetch(urlfor + props.selectedDebate.id)
    .then(res => {
      return res.json();
    })
    .then(data => {
      props.setArgsPour(data);
    });
  fetch(urlagainst + props.selectedDebate.id)
    .then(res => {
      return res.json();
    })
    .then(data => {
      props.setArgsContre(data);
    });
}

const useStyle = makeStyles(props => ({
  container: {
    width: "100%",
    backgroundColor: body_color,
    height: "100%",
    padding: "5px"
  },
  flexContainer: { display: "flex" },
  side: {
    width: props.writingArg === NONE ? "50%" : "100%",
    display: "inline-block",
    flexGrow: "1",
    margin: "10px",
    borderRadius: "20px",
    padding: "5px"
  },
  switchArgs: { width: "100%" },
  button: {
    width: "50%",
    color: buttons_color,
    border: "1px solid rgba(0, 0, 0, 0.23)"
  }
}));

function ConnectedContainerDebate(props) {
  const classes = useStyle(props);
  return (
    <div className={classes.container}>
      <div className={classes.switchArgs}>
        <Button
          variant="outlined"
          color="secondary"
          className={classes.button}
          startIcon={<WhatshotIcon />}
          onClick={() =>
            getSortedArgs(
              GET_SORTED_ARGSFOR_HOT,
              GET_SORTED_ARGSAGAINST_HOT,
              props
            )
          }
        >
          HOT
        </Button>
        <Button
          variant="outlined"
          color="primary"
          className={classes.button}
          startIcon={<AcUnitIcon />}
          onClick={() =>
            getSortedArgs(
              GET_SORTED_ARGSFOR_FRESH,
              GET_SORTED_ARGSAGAINST_FRESH,
              props
            )
          }
        >
          FRESH
        </Button>
      </div>
      <div className={classes.flexContainer}>
        <div
          className={classes.side}
          style={{
            backgroundColor: for_color
          }}
        >
          <Opinions side={FOR}></Opinions>
        </div>
        <div
          className={classes.side}
          style={{
            backgroundColor: against_color
          }}
        >
          <Opinions side={AGAINST}></Opinions>
        </div>
      </div>
    </div>
  );
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ConnectedContainerDebate);
