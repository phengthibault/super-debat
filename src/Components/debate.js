import React, { Component } from "react";
import NavBar from "./navbar";
import HeaderDebate from "./headerDebate";
import {
  GET_DEBATE_FROM_ID,
  GET_ARGSAGAINST_FROM_DEBATE_ID,
  GET_ARGSFOR_FROM_DEBATE_ID
} from "../Constants/urlApi";
import setSelectedDebate from "../Actions/setSelectedDebate";
import { connect } from "react-redux";
import ContainerDebate from "./containerDebate";
import { setArgsPour, setArgsContre } from "../Actions/setArgs";
import { body_color } from "../CSS/color";

function mapStateToProps(state) {
  return { debates: state.debates };
}

function mapDispatchToProps(dispatch) {
  return {
    setSelectedDebate: debate => dispatch(setSelectedDebate(debate)),
    setArgsPour: argsFor => dispatch(setArgsPour(argsFor)),
    setArgsContre: argsAgainst => dispatch(setArgsContre(argsAgainst))
  };
}

class ConnectedDebate extends Component {
  //When this component is loaded try to set the selected debates from the debates already in the store or fetch it
  constructor(props) {
    super(props);
    this.setArgs = this.setArgs.bind(this);
  }

  setArgs(id) {
    fetch(GET_ARGSAGAINST_FROM_DEBATE_ID + id)
      .then(response => {
        return response.json();
      })
      .then(argsAgainst => {
        this.props.setArgsContre(argsAgainst);
      });
    fetch(GET_ARGSFOR_FROM_DEBATE_ID + id)
      .then(response => {
        return response.json();
      })
      .then(argsFor => {
        this.props.setArgsPour(argsFor);
      });
  }

  componentDidMount() {
    let id = this.props.match.params.id; //this.props.match.params.id; TODO Remplacer l'id pour le prendre de l'url
    console.log("id" + id);
    let selectedDebate = this.props.debates.find(debate => debate.id == id);
    console.log(selectedDebate);
    if (selectedDebate == undefined) {
      console.log("Ajax Request");
      fetch(GET_DEBATE_FROM_ID + id)
        .then(response => {
          return response.json();
        })
        .then(debate => {
          selectedDebate = debate;
        });
    }
    this.props.setSelectedDebate(selectedDebate);
    this.setArgs(selectedDebate.id);
  }

  render() {
    return (
      <div style={{ backgroundColor: body_color, height: "100vh" }}>
        <NavBar></NavBar>
        <HeaderDebate></HeaderDebate>
        <ContainerDebate></ContainerDebate>
      </div>
    );
  }
}

const Debate = connect(mapStateToProps, mapDispatchToProps)(ConnectedDebate);
export default Debate;
