import React from "react";

export default function Comment(props) {
  const { name, content, dateFormat } = props.comment;

  return (
    <div className="media mb-10">
      <img
        className="mr-10 bg-light rounded"
        width="48"
        height="48"
        alt={name}
      />

      <div className="media-body p-2 shadow-sm rounded bg-light border">
        <small className="float-right text-muted">{dateFormat}</small>
        <h6 className="mt-0 mb-1 text-muted">{name}</h6>
        {content}
      </div>
    </div>
  );
}
