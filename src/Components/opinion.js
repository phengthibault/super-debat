import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import ProfileIcon from "./profileIcon";
import InteractBar from "./interactBar";
import { connect } from "react-redux";
import { NONE } from "../Constants/utiles";

function mapStateToProps(state) {
  return {
    writingMode: state.writingArg
  };
}

const useStyles = makeStyles({
  card: {
    minWidth: 275,
    height: 250,
    margin: "5px",
    display: "flex",
    flexDirection: "column"
  },
  cardContent: { height: "75%", overflowY: "auto" }
});

function ConnectedOpinion(props) {
  const classes = useStyles();
  const bull = <span className={classes.bullet}>•</span>;

  return (
    <div>
      <Card className={classes.card}>
        <CardContent className={classes.cardContent}>
          <ProfileIcon position="none" user={props.arg.autor}></ProfileIcon>
          <Typography variant="body1" component="p">
            {props.arg.content}
          </Typography>
        </CardContent>
        <InteractBar content={props.arg} type="arg"></InteractBar>
      </Card>
    </div>
  );
}

export default connect(mapStateToProps)(ConnectedOpinion);
