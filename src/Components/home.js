import React, { Component } from "react";
import NavBar from "./navbar";
import ContainerTopDebats from "./containerTopDebats";
import WelcomingBand from "./welcomingBand";

class Home extends Component {
  render() {
    return (
      <div>
        <NavBar></NavBar>
        <WelcomingBand></WelcomingBand>
        <ContainerTopDebats></ContainerTopDebats>
      </div>
    );
  }
}

export default Home;
