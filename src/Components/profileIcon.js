import React, { Component } from "react";
import { connect } from "react-redux";
import Avatar from "@material-ui/core/Avatar";
import IconButton from "@material-ui/core/IconButton";
import { fade, makeStyles } from "@material-ui/core/styles";
import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";
import AccountCircle from "@material-ui/icons/AccountCircle";
import { setisUserLog, setUser } from "../Actions/setUser";

import { BrowserRouter as Router, Link } from "react-router-dom";

function mapStateToProps(state) {
  return {
    isUserLog: state.isUserLog
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setisUserLog: isUserLog => dispatch(setisUserLog(isUserLog))
  };
}

const useStyles = makeStyles(theme => ({
  search: {
    position: "relative",
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    "&:hover": {
      backgroundColor: fade(theme.palette.common.white, 0.25)
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing(3),
      width: "auto"
    }
  },
  sectionDesktop: {
    display: "none",
    [theme.breakpoints.up("md")]: {
      display: "flex"
    }
  },
  searchIcon: {
    width: theme.spacing(7),
    height: "100%",
    position: "absolute",
    pointerEvents: "none",
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  inputRoot: {
    color: "inherit"
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 7),
    transition: theme.transitions.create("width"),
    width: "30%",
    [theme.breakpoints.up("md")]: {
      width: 200
    }
  }
}));

function ProfileIcon(props) {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);

  const isMenuOpen = Boolean(anchorEl);

  const handleProfileMenuOpen = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
  };

  const menuId = "primary-search-account-menu";

  const getItemsMenu = () => {
    if (props.currentUserMode && props.user != null) {
      if (props.isUserLog) {
        return (
          <div>
            <MenuItem component={Link} to={`/profile/${props.user.id}`}>
              Profile
            </MenuItem>
            <MenuItem component={Link} to={"/myAccount"}>
              My account
            </MenuItem>
            <MenuItem
              onClick={() => {
                handleMenuClose();
                props.setisUserLog(false);
                localStorage.removeItem("sessionID");
              }}
            >
              Log Out
            </MenuItem>
          </div>
        );
      } else {
        return (
          <div>
            <MenuItem onClick={handleMenuClose} component={Link} to={"/Login"}>
              Login
            </MenuItem>
            <MenuItem
              onClick={handleMenuClose}
              component={Link}
              to={"/Register"}
            >
              Register
            </MenuItem>
          </div>
        );
      }
    } else {
      if (props.user != undefined && props.user != null) {
        return (
          <div>
            <MenuItem component={Link} to={`/profile/${props.user.id}`}>
              Profile
            </MenuItem>
          </div>
        );
      }
    }
  };
  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{ vertical: "top", horizontal: "right" }}
      id={menuId}
      keepMounted
      transformOrigin={{ vertical: "top", horizontal: "right" }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      {getItemsMenu()}
    </Menu>
  );

  const displayMenu = () => {
    console.log(props.user);
    if (props.user != undefined && props.user != null) {
      if (Object.keys(props.user).length != 0 || props.currentUserMode) {
        return (
          <div className={props.className}>
            <IconButton
              edge="end"
              style={{ position: props.position, float: props.side }}
              aria-label="account of current user"
              aria-controls={menuId}
              aria-haspopup="true"
              onClick={handleProfileMenuOpen}
              color="secondary"
            >
              {props.currentUserMode && props.isUserLog === false ? (
                <AccountCircle></AccountCircle>
              ) : (
                <Avatar>
                  {props.user.surName[0] + props.user.lastName[0]}
                </Avatar>
              )}
            </IconButton>
            {renderMenu}
          </div>
        );
      } else {
        return <div></div>;
      }
    } else {
      return <div></div>;
    }
  };

  return displayMenu();
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfileIcon);
