import React from "react";
import { withRouter } from "react-router-dom";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import { useState } from "react";


var axios = require("axios");
var errorMessage=true;


const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: "100px",
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(3)
  },
  submit: {
    margin: theme.spacing(10, 0, 2)
  }
}));
/*
function submitOrder(data) {
  const url = "http://localhost:8082/user";
  console.log("submit");
  let that = this;

  //console.log(this.state);
  console.log(data);

  const response = fetch(url, {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(data)
  }).then(response => {
    console.log(response);
    if (response.ok) {
      alert("utilisateur ajouter :" + data.login);
      that.props.history.push("/Login");
    } else {
      response.json().then(err => Promise.reject(err));
      alert("error", response);
    }
  });
}*/





function submitOrder(dataForm, props, setValues) {
  if(dataForm.email === ""){
    setValues({ ...dataForm, errorMessageEmail: true });
    console.log(dataForm);
    }

  if(dataForm.pwd === ""){
    setValues({ ...dataForm, errorMessagePwd: true });
    console.log(dataForm);
    }

  if(dataForm.surName === ""){
    setValues({ ...dataForm, errorMessageSurName: true });
    console.log(dataForm);
    }

  if(dataForm.lastName === ""){
    setValues({ ...dataForm, errorMessageLastName: true });
    console.log(dataForm);
    }
    if(dataForm.login === ""){
      setValues({ ...dataForm, errorMessageLogin: true });
      console.log(dataForm);
      }
  else{
    
  
    axios({
      method: "POST",
      url: "http://localhost:8082/user",
      data: dataForm
    })
      .then(function(response) {
        console.log(response);

        if (response.status == 200) {
          alert(response.data);
          props.history.push("/Login");
        } else {
          alert("error", response);
        }
      })
      .catch(function(error) {
        console.log(error);
      });
  }
}

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit" href="/">
        Super Debat
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

function UserForm(props) {
  //   this.props.submitUserHandler(JSON.stringify(this.state));

  const classes = useStyles();

  const handleInputChange = e => {
    const { name, value } = e.target;
    setValues({ ...values, [name]: value });
  };

  const [values, setValues] = useState({
    surName: "",
    lastName: "",
    email: "",
    imgUrl: "",
    login: "",
    pwd: "",
    errorMessageEmail:false,
    errorMessagePwd: false,
    errorMessageSurName:false,
    errorMessageLastName: false,
    errorMessageLogin: false
  });

  


/*async registerFunction(){
    var responseAddUser = await addUser(this.state.user)
    if(responseAddUser.status === 200){
        this.setState({
            registered: true,
        })
    }
    else{
        this.setState({
            errorMessage: true,
        })
    }
}*/
















  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign up
        </Typography>
        <Grid container spacing={2}>
          <Grid item xs={12} sm={6}>
            <TextField
              autoComplete="fname"
              name="surName"
              variant="outlined"
              required
              fullWidth
              id="surName"
              label="surName"
              autoFocus
              onChange={handleInputChange}
              value={values.surName}
            />
          </Grid>

        

          <Grid item xs={12} sm={6}>
            <TextField
              variant="outlined"
              required
              fullWidth
              id="lastName"
              label="Last Name"
              name="lastName"
              autoComplete="lname"
              onChange={handleInputChange}
              value={values.lastName}
            />
          </Grid>

          {values.errorMessageSurName ? 
                (
                    <Typography variant="body1" color="error">Error! Invalid surName</Typography>
                    
                ) : (<div></div>)
          }

          {values.errorMessageLastName ? 
                (
                    <Typography variant="body1" color="error">Error! Invalid lastName</Typography>
                    
                ) : (<div></div>)
          }

          <Grid item xs={12}>
            <TextField
              variant="outlined"
              required
              fullWidth
              id="email"
              label="Email Address"
              name="email"
              autoComplete="email"
              onChange={handleInputChange}
              value={values.email}
              type="email"
            />
          </Grid>

          {values.errorMessageEmail ? 
                (
                    <Typography variant="body1" color="error">Error! Invalid Email</Typography>
                    
                ) : (<div></div>)
          }
          
          <Grid item xs={12}>
            <TextField
              variant="outlined"
              required
              fullWidth
              id="login"
              label="login"
              name="login"
              autoComplete="Login"
              onChange={handleInputChange}
              value={values.login}
            />
          </Grid>

            {values.errorMessageLogin ? 
                (
                    <Typography variant="body1" color="error">Error! Invalid login</Typography>
                    
                ) : (<div></div>)
          }

          <Grid item xs={12}>
            <TextField
              variant="outlined"
              required
              fullWidth
              name="pwd"
              label="pwd"
              type="password"
              id="pwd"
              autoComplete="current-password"
              onChange={handleInputChange}
              value={values.pwd}
            />
          </Grid>

          {values.errorMessagePwd ? 
                (
                    <Typography variant="body1" color="error">Error! Invalid password</Typography>
                    
                ) : (<div></div>)
                }

          <Grid item xs={12}>
            <TextField
              variant="outlined"
              fullWidth
              name="imgUrl"
              label="imgUrl"
              id="imgUrl"
              autoComplete="Image Url"
              onChange={handleInputChange}
              value={values.imgUrl}
            />
          </Grid>
        </Grid>
        <Button
          type="submit"
          fullWidth
          variant="contained"
          color="primary"
          // className={classes.submit}
          onClick={() => submitOrder(values, props, setValues)}
        >
          Sign Up
        </Button>

        
        
        


        <Grid container justify="flex-end">
          <Grid item>
            <Link href="/Login" variant="body2">
              Already have an account? Sign in
            </Link>
          </Grid>
        </Grid>
      </div>
      <Box mt={5}>{Copyright()}</Box>
    </Container>
  );
}
export default withRouter(UserForm);
