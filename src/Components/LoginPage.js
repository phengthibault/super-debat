import React, { Component } from "react";
import { connect } from "react-redux";
import { setisUserLog, setUser } from "../Actions/setUser";
import { withRouter } from "react-router-dom";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";

var axios = require("axios");

function mapDispatchToProps(dispatch) {
  return {
    setUser: user => dispatch(setUser(user))
  };
}

class ConnectedLoginPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      id: "",
      login: "",
      password: "",
      port: "8082"
    };
    this.getUserInformations = this.getUserInformations.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  validateForm() {
    return this.state.login.length > 0 && this.state.password.length > 0;
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  };

  getUserInformations(id) {
    let that = this;
    fetch("http://localhost:" + this.state.port + "/user/" + id)
      .then(function(response) {
        return response.json();
      })
      .then(function(user) {
        that.props.setUser(user);

        that.props.history.push("/");
      });
  }

  handleSubmit = event => {
    event.preventDefault();
  };

  getUserId = event => {
    axios
      .post("http://localhost:" + this.state.port + "/auth", {
        login: this.state.login,
        pwd: this.state.password
      })
      .then(
        function(data) {
          if (data.data) {
            alert("Vous etes connecté!");
            localStorage.setItem("sessionID", data.data.sessionID);

            this.getUserInformations(data.data.id);
          } else {
            alert("go register");
          }
        }.bind(this)
      )
      .catch(function(error) {
        console.log(error);
      });
  };

  render() {
    return (
      <div className="Login">
        <form
          onSubmit={this.handleSubmit}
          style={{ position: "absolute", top: "40%", left: "45%" }}
        >
          <TextField
            style={{ display: "block" }}
            id="login"
            label="Login"
            value={this.state.login}
            onChange={this.handleChange}
            margin="normal"
            color="primary"
          />
          <TextField
            style={{ display: "block" }}
            id="password"
            label="Password"
            type="password"
            value={this.state.password}
            onChange={this.handleChange}
            margin="normal"
            color="primary"
          />

          <Button color="primary" onClick={e => this.getUserId(e)}>
            Connect
          </Button>
        </form>
      </div>
    );
  }
}

const LoginPage = connect(null, mapDispatchToProps)(ConnectedLoginPage);

export default withRouter(LoginPage);
