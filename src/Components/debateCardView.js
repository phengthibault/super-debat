import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import clsx from "clsx";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Collapse from "@material-ui/core/Collapse";
import Avatar from "@material-ui/core/Avatar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import { red, green } from "@material-ui/core/colors";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import ProfileIcon from "./profileIcon";
import TopComment from "./topComment";
import InteractBar from "./interactBar";
import { Link } from "react-router-dom";

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
    "& > *": {
      margin: theme.spacing(1)
    }
  },
  card: {
    maxWidth: 500,
    height: "100%",
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
    position: "relative"
  },
  media: {
    height: 0,
    paddingTop: "56.25%" // 16:9
  },
  expand: {
    transform: "rotate(0deg)",
    marginLeft: "auto",
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.shortest
    })
  },
  expandOpen: {
    transform: "rotate(180deg)"
  },
  avatar: {
    backgroundColor: red[500]
  },
  percentageAgainst: {
    color: theme.palette.getContrastText(red[500]),
    backgroundColor: red[500]
  },
  percentageFor: {
    color: "#fff",
    backgroundColor: green[500]
  },
  flexGrow: {
    "flex-grow": 1,
    textAlign: "center",
    fontWeight: "bold"
  }
}));

export default function DebateCardView(props) {
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };
  console.log(props.autorDebate);
  console.trace();
  return (
    <Card className={classes.card}>
      <CardHeader
        avatar={<ProfileIcon user={props.debate.autor} />}
        title={
          <Link
            style={{ textDecoration: "none", color: "black" }}
            to={`/debate/${props.debate.id}`}
          >
            {props.debate.title}
          </Link>
        }
        subheader={props.debate.date}
        style={{ position: "absolute", top: 0 }}
      />
      <div style={{ marginTop: "100px" }}>
        <CardMedia
          className={classes.media}
          image={props.debate.imgUrl}
          title={props.debate.imgTitle}
        />
        <CardContent>
          <Typography variant="body2" color="textSecondary" component="p">
            {props.debate.describe}
          </Typography>
        </CardContent>
      </div>
      <CardActions disableSpacing>
        <InteractBar content={props.debate} type="debate"></InteractBar>
        <IconButton
          className={clsx(classes.expand, {
            [classes.expandOpen]: expanded
          })}
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="show more"
        >
          <ExpandMoreIcon />
        </IconButton>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
          <div className={classes.root}>
            <Avatar className={classes.percentageFor}>
              {props.debate.percentageAgainst}%
            </Avatar>
            <span className={classes.flexGrow}>VS</span>
            <Avatar className={classes.percentageAgainst}>
              {props.debate.percentageFor}%
            </Avatar>
          </div>
          <TopComment topArg={props.topArg} />
        </CardContent>
      </Collapse>
    </Card>
  );
}
