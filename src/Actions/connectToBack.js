import { connect } from "react-redux";

const axios = require("axios");
/*
function mapDispatchToProps(dispatch) {
  return {
    setProfile: profile => dispatch(setProfile(profile))
  };
}*/

export const getDebats = async () => {
  return axios
    .get("http://localhost:8082/Debats")
    .then(function(response) {
      console.log(response);
      return response.data;
    })
    .catch(function(error) {
      console.log(error);
    });
};

export const postDebat = dataForm => {
  axios({
    method: "POST",
    url: "http://localhost:8082/user",
    data: dataForm
  })
    .then(function(response) {
      console.log(response);
    })
    .catch(function(error) {
      console.log(error);
    });
};

/*
const ConnectToStore = connect(mapDispatchToProps)(getDebats);
export default ConnectToStore;
*/
