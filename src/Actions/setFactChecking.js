import { SET_FACT_CHECKING } from "../Constants/actions";
export default function setFactChecking(payload) {
  let argsFor = payload.argsFor;
  let argsAgainst = payload.argsAgainst;
  const addFactChecking = function(args) {
    return args.map(arg => {
      if (arg.id === payload.id) {
        if (payload.side === "for") {
          arg.factCheckings.For.push(payload);
        } else {
          arg.factCheckings.Against.push(payload);
        }
      }
      return arg;
    });
  };
  argsFor = addFactChecking(argsFor);
  argsAgainst = addFactChecking(argsAgainst);
  payload = { argsFor: argsFor, argsAgainst: argsAgainst };
  return { type: SET_FACT_CHECKING, payload };
}
