import { SET_WRITING_MODE } from "../Constants/actions";
export default function setWritingMode(payload) {
  return { type: SET_WRITING_MODE, payload };
}
