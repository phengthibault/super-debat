import { UPDATE_LIKE } from "../Constants/actions";
import { store } from "../Store/store";
import SelectInput from "@material-ui/core/Select/SelectInput";

function removeFromArray(array, removed) {
  const returnArray = [];
  for (let i = 0; i < array.length; i++) {
    if (array[i] !== removed) {
      returnArray.push(array[i]);
    }
  }
  return returnArray;
}

function updateLikeNumber(array, id, value) {
  console.log(array, id, value);
  return array.map(e => {
    if (e.id === id) {
      console.log(e.numberLikes);
      e.numberLikes = e.numberLikes + value;
      console.log(e.numberLikes);
    }
    return e;
  });
}

function updateDislikeNumber(array, id, value) {
  return array.map(e => {
    if (e.id === id) {
      e.numberDislikes = e.numberDislikes + value;
    }
    return e;
  });
}

export default function updateLike(props) {
  let value;
  const state = JSON.parse(JSON.stringify(store.getState()));
  if (props.typeLike === "like" && props.type === "arg") {
    if (props.currentUser.likesArgs.includes(props.id)) {
      state.user.likesArgs = removeFromArray(state.user.likesArgs, props.id);
      value = -1;
    } else {
      state.user.likesArgs.push(props.id);
      value = 1;
    }
    state.argsFor = updateLikeNumber(state.argsFor, props.id, value);
    state.argsAgainst = updateLikeNumber(state.argsAgainst, props.id, value);
  } else if (props.typeLike === "like" && props.type === "debate") {
    if (props.currentUser.likesDebate.includes(props.id)) {
      state.user.likesDebate = removeFromArray(
        state.user.likesDebate,
        props.id
      );
      value = -1;
    } else {
      state.user.likesDebate.push(props.id);
      value = 1;
    }
    state.selectedDebate.numberLikes += value;
    let totalLike =
      state.selectedDebate.numberLikes + state.selectedDebate.numberDislikes;
    state.selectedDebate.percentageFor = Math.floor(
      (state.selectedDebate.numberLikes * 100) / totalLike
    );
    state.selectedDebate.percentageAgainst = Math.floor(
      (state.selectedDebate.numberDislikes * 100) / totalLike
    );
  } else if (props.typeLike === "dislike" && props.type === "arg") {
    if (props.currentUser.dislikesArgs.includes(props.id)) {
      state.user.dislikesArgs = removeFromArray(
        state.user.dislikesArgs,
        props.id
      );
      value = -1;
    } else {
      state.user.dislikesArgs.push(props.id);
      value = 1;
    }
    state.argsFor = updateDislikeNumber(state.argsFor, props.id, value);
    state.argsAgainst = updateDislikeNumber(state.argsAgainst, props.id, value);
  } else if (props.typeLike === "dislike" && props.type === "debate") {
    if (props.currentUser.dislikesDebate.includes(props.id)) {
      state.user.dislikesDebate = removeFromArray(
        state.user.dislikesDebate,
        props.id
      );
      value = -1;
    } else {
      state.user.dislikesDebate.push(props.id);
      value = 1;
    }
    state.selectedDebate.numberDislikes += value;
    let totalLike =
      state.selectedDebate.numberLikes + state.selectedDebate.numberDislikes;
    state.selectedDebate.percentageFor = Math.floor(
      (state.selectedDebate.numberLikes * 100) / totalLike
    );
    state.selectedDebate.percentageAgainst = Math.floor(
      (state.selectedDebate.numberDislikes * 100) / totalLike
    );
  }
  let payload = state;
  return { type: UPDATE_LIKE, payload };
}
