import { SET_PROFILE } from "../Constants/actions";
export default function setProfile(payload) {
  return { type: SET_PROFILE, payload };
}
