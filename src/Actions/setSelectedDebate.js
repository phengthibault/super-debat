import { SET_SELECTED_DEBATE } from "../Constants/actions";
export default function setSelectedDebate(payload) {
  return { type: SET_SELECTED_DEBATE, payload };
}
