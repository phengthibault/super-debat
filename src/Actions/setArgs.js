import { SET_ARGSPOUR, SET_ARGSCONTRE } from "../Constants/actions";
export function setArgsPour(payload) {
  return { type: SET_ARGSPOUR, payload };
}

export function setArgsContre(payload) {
  return { type: SET_ARGSCONTRE, payload };
}
