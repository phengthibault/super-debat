import { SET_DEBATES } from "../Constants/actions";
export default function setDebates(payload) {
  return { type: SET_DEBATES, payload };
}
