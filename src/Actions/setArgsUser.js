import { SET_ARGSUSER } from "../Constants/actions";
export default function setArgsUser(payload) {
  return { type: SET_ARGSUSER, payload };
}
