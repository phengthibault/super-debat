import React, { Component } from "react";
import "./App.css";
import { Provider } from "react-redux";
import { store, persistor } from "./Store/store";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { PersistGate } from "redux-persist/integration/react";
import Home from "./Components/home";
import Debate from "./Components/debate";
import LoginPage from "./Components/LoginPage";
import Navbar from "./Components/navbar";
import UserForm from "./Components/UserForm";
import SettingHome from "./Components/SettingHome";
import Set from "./Components/SettingHome";
import CreateDebat from "./Components/creationDebat";
import PrivateRoute from "./Components/PrivateRoute";
import UserProfileDisplay from "./Components/UserProfileDisplay";

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <Switch>
            <Route exact path="/">
              <Home />
            </Route>

            <Route exact path="/Login">
              <Navbar />
              <LoginPage />
            </Route>
            <PrivateRoute
              path="/myAccount"
              component={SettingHome}
            ></PrivateRoute>

            <Route exact path="/Register">
              <Navbar />
              <UserForm />
            </Route>
            <Route exact path="/Create">
              <Navbar />
              <CreateDebat />
            </Route>

            <Route
              exact
              path="/profile/:id"
              component={UserProfileDisplay}
            ></Route>
            <Route exact path="/debate/:id" component={Debate}></Route>
          </Switch>
        </Router>
        {/*</PersistGate>*/}
      </Provider>
    );
  }
}

export default App;
