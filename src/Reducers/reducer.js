import {
  SET_DEBATES,
  SET_SELECTED_DEBATE,
  SET_DEBAT,
  SET_ARGSPOUR,
  SET_ARGSCONTRE,
  SET_USER,
  SET_FACTCHEKING,
  SET_WRITING_MODE,
  UPDATE_LIKE,
  SET_ISUSERLOG,
  SET_PROFILE,
  SET_FACT_CHECKING,
  SET_ARGSUSER
} from "../Constants/actions";
import { NONE } from "../Constants/utiles";

const initialState = {
  debates: [
    {
      title: "Debat sur les retraites",
      autor: { lastName: "Ecran", surName: "Guillaume", id: 15 },
      keywordsFor: ["Injustice Sociale", "Retraite 50 ans"],
      keywordsAgainst: ["Démographie différentes", "Justice Sociale"],
      topArg: {
        content: "Ces sommes servent ensuite à payer les pensions de retraite.",
        id: 16,
        numberLikes: 12,
        numberDislikes: 14,
        autor: {},
        keyWords: ["retraite", "salaire"],
        idsFactChecking: [17],
        side: false
      },
      date: "28/07/17",
      percentageFor: 40,
      percentageAgainst: 60,
      id: 15,
      numberLikes: 20,
      numberDislikes: 150,
      imgUrl:
        "https://s.ftcdn.net/v2013/pics/all/curated/RKyaEDwp8J7JKeZWQPuOVWvkUjGQfpCx_cover_580.jpg?r=1a0fc22192d0c808b8bb2b9bcfbf4a45b1793687",
      imgTitle: "Manifestation contre la reforme des retraites, France",
      describe: "La grève du 5 décembre.",
      numberArgs: 1167
    },
    {
      title: "Debat sur les retraites",
      autor: { lastName: "Ecran", surName: "Guillaume", id: 16 },
      keywordFor: ["Injustice Sociale", "Retraite 50 ans"],
      keywordAgainst: ["Démographie différentes", "Justice Sociale"],
      topArg: {
        content:
          "Le système de retraite français fonctionne comme une assurance collective. Les travailleurs (et les employeurs) financent les caisses de retraite en s’acquittant de cotisations prélevées sur leurs revenus. Ces sommes servent ensuite à payer les pensions de retraite.",
        id: 16,
        numberLikes: 12,
        numberDislikes: 14,
        autor: { lastName: "Ecran", surName: "Guillaume", id: 16 },
        keyWords: ["retraite", "salaire"],
        idsFactChecking: [17],
        side: false
      },
      date: "28/07/17",
      percentageFor: 40,
      percentageAgainst: 60,
      id: 15,
      numberLikes: 20,
      numberDislikes: 150,
      imgUrl:
        "https://static.lexpress.fr/medias_12190/w_2048,h_1146,c_crop,x_0,y_215/w_480,h_270,c_fill,g_north/v1578081234/manifestation-a-paris-contre-la-reforme-des-retraites-le-28-decembre-2019-1_6241738.jpg",
      imgTitle: "Manifestation contre la reforme des retraites, France",
      describe:
        "C’est « la mère de toutes les réformes », « une révolution », un véritable « big bang ». A moins que ce ne soit un « mirage », « une catastrophe », ou même le « hold-up du siècle ». Les promoteurs comme les contempteurs de la réforme des retraites voulue par le gouvernement n’hésitent pas à abuser des superlatifs pour qualifier la mesure, à la veille de la grève du 5 décembre.",
      numberArgs: 1167
    },
    {
      title: "Debat sur les retraites",
      autor: { lastName: "Ecran", surName: "Guillaume", id: 16 },
      keywordsFor: ["Injustice Sociale", "Retraite 50 ans"],
      keywordsAgainst: ["Démographie différentes", "Justice Sociale"],
      topArg: {
        content:
          "Le système de retraite français fonctionne comme une assurance collective. Les travailleurs (et les employeurs) financent les caisses de retraite en s’acquittant de cotisations prélevées sur leurs revenus. Ces sommes servent ensuite à payer les pensions de retraite.",
        id: 16,
        numberLikes: 12,
        numberDislikes: 14,
        autor: { lastName: "Ecran", surName: "Guillaume", id: 16 },
        keyWords: ["retraite", "salaire"],
        idsFactChecking: [17],
        side: false
      },
      date: "28/07/17",
      percentageFor: 40,
      percentageAgainst: 60,
      id: 15,
      numberLikes: 20,
      numberDislikes: 150,
      imgUrl:
        "https://static.lexpress.fr/medias_12190/w_2048,h_1146,c_crop,x_0,y_215/w_480,h_270,c_fill,g_north/v1578081234/manifestation-a-paris-contre-la-reforme-des-retraites-le-28-decembre-2019-1_6241738.jpg",
      imgTitle: "Manifestation contre la reforme des retraites, France",
      describe:
        "C’est « la mère de toutes les réformes », « une révolution », un véritable « big bang ». A moins que ce ne soit un « mirage », « une catastrophe », ou même le « hold-up du siècle ». Les promoteurs comme les contempteurs de la réforme des retraites voulue par le gouvernement n’hésitent pas à abuser des superlatifs pour qualifier la mesure, à la veille de la grève du 5 décembre.",
      numberArgs: 1167
    },
    {
      title: "Debat sur les retraites",
      autor: { lastName: "Ecran", surName: "Guillaume", id: 16 },
      keywordsFor: ["Injustice Sociale", "Retraite 50 ans"],
      keywordsAgainst: ["Démographie différentes", "Justice Sociale"],
      topArg: {
        content:
          "Le système de retraite français fonctionne comme une assurance collective. Les travailleurs (et les employeurs) financent les caisses de retraite en s’acquittant de cotisations prélevées sur leurs revenus. Ces sommes servent ensuite à payer les pensions de retraite.",
        id: 16,
        numberLikes: 12,
        numberDislikes: 14,
        autor: { lastName: "Ecran", surName: "Guillaume", id: 16 },
        keyWords: ["retraite", "salaire"],
        idsFactChecking: [17],
        side: false
      },
      date: "28/07/17",
      percentageFor: 40,
      percentageAgainst: 60,
      id: 15,
      numberLikes: 20,
      numberDislikes: 150,
      imgUrl:
        "https://static.lexpress.fr/medias_12190/w_2048,h_1146,c_crop,x_0,y_215/w_480,h_270,c_fill,g_north/v1578081234/manifestation-a-paris-contre-la-reforme-des-retraites-le-28-decembre-2019-1_6241738.jpg",
      imgTitle: "Manifestation contre la reforme des retraites, France",
      describe:
        "C’est « la mère de toutes les réformes », « une révolution », un véritable « big bang ». A moins que ce ne soit un « mirage », « une catastrophe », ou même le « hold-up du siècle ». Les promoteurs comme les contempteurs de la réforme des retraites voulue par le gouvernement n’hésitent pas à abuser des superlatifs pour qualifier la mesure, à la veille de la grève du 5 décembre.",
      numberArgs: 1167
    },
    {
      title: "Debat sur les retraites",
      autor: { lastName: "Ecran", surName: "Guillaume", id: 16 },
      keywordsFor: ["Injustice Sociale", "Retraite 50 ans"],
      keywordsAgainst: ["Démographie différentes", "Justice Sociale"],
      topArg: {
        content:
          "Le système de retraite français fonctionne comme une assurance collective. Les travailleurs (et les employeurs) financent les caisses de retraite en s’acquittant de cotisations prélevées sur leurs revenus. Ces sommes servent ensuite à payer les pensions de retraite.",
        id: 16,
        numberLikes: 12,
        numberDislikes: 14,
        autor: { lastName: "Ecran", surName: "Guillaume", id: 16 },
        keyWords: ["retraite", "salaire"],
        idsFactChecking: [17],
        side: false
      },
      date: "28/07/17",
      percentageFor: 50,
      percentageAgainst: 50,
      id: 16,
      numberLikes: 20,
      numberDislikes: 150,
      imgUrl:
        "https://static.lexpress.fr/medias_12190/w_2048,h_1146,c_crop,x_0,y_215/w_480,h_270,c_fill,g_north/v1578081234/manifestation-a-paris-contre-la-reforme-des-retraites-le-28-decembre-2019-1_6241738.jpg",
      imgTitle: "Manifestation contre la reforme des retraites, France",
      describe:
        "C’est « la mère de toutes les réformes », « une révolution », un véritable « big bang ». A moins que ce ne soit un « mirage », « une catastrophe », ou même le « hold-up du siècle ». Les promoteurs comme les contempteurs de la réforme des retraites voulue par le gouvernement n’hésitent pas à abuser des superlatifs pour qualifier la mesure, à la veille de la grève du 5 décembre.",
      numberArgs: 1167
    }
  ],
  argsFor: [
    {
      content: "Mon texte de contenu",
      id: 16,
      numberLikes: 7,
      numberDislikes: 5,
      autor: { lastName: "Landeroin", surName: "Clement", id: 15 },
      keyWords: ["retraite", "salaire"],
      dateFormat: "02-01-2020",
      factCheckings: {
        For: [
          {
            autor: { lastName: "Ecran", surName: "Guillaume", id: 16 },
            url: "https://checkinformation.com",
            content:
              "Here you will find the proof that this argument is bullshit.1",
            title: "Mon titre"
          },
          {
            autor: {},
            url: "https://checkinformation.com",
            content:
              "Here you will find the proof that this argument is bullshit.2",
            title: "Mon titre"
          }
        ],
        Against: [
          {
            autor: { lastName: "Ecran", surName: "Guillaume", id: 16 },
            url: "https://checkinformation.com",
            content:
              "Here you will find the proof that this argument is bullshit.1",
            title: "Mon titre"
          },
          {
            autor: {},
            url: "https://checkinformation.com",
            content:
              "Here you will find the proof that this argument is bullshit.2",
            title: "Mon titre"
          }
        ]
      }
    },
    {
      content:
        "C’est « la mère de toutes les réformes », « une révolution », un véritable « big bang ». A moins que ce ne soit un « mirage », « une catastrophe », ou même le « hold-up du siècle ». Les promoteurs comme les contempteurs de la réforme des retraites voulue par le gouvernement n’hésitent pas à abuser des superlatifs pour qualifier la mesure, à la veille de la grève du 5 décembre.",
      id: 4,
      numberLikes: 14,
      numberDislikes: 15,
      autor: { lastName: "Landeroin", surName: "Clement", id: 15 },
      keyWords: ["retraite", "salaire"],
      factCheckings: {
        For: [
          {
            autor: { lastName: "Ecran", surName: "Guillaume", id: 16 },
            url: "https://checkinformation.com",
            content:
              "Here you will find the proof that this argument is bullshit.1",
            title: "Mon titre"
          },
          {
            autor: {},
            url: "https://checkinformation.com",
            content:
              "Here you will find the proof that this argument is bullshit.2",
            title: "Mon titre"
          }
        ],
        Against: [
          {
            autor: { lastName: "Ecran", surName: "Guillaume", id: 16 },
            url: "https://checkinformation.com",
            content:
              "Here you will find the proof that this argument is bullshit.1",
            title: "Mon titre"
          },
          {
            autor: {},
            url: "https://checkinformation.com",
            content:
              "Here you will find the proof that this argument is bullshit.2",
            title: "Mon titre"
          }
        ]
      }
    }
  ],
  argsAgainst: [
    {
      content:
        "Mon texte de contenu Mon texte de contenu Mon texte de contenu Mon texte de contenu Mon texte de contenu Mon texte de contenu Mon texte de contenu Mon texte de contenu Mon texte de contenu Mon texte de contenu Mon texte de contenu Mon texte de contenu Mon texte de contenu Mon texte de contenu Mon texte de contenu Mon texte de contenu Mon texte de contenu Mon texte de contenu Mon texte de contenu Mon texte de contenu Mon texte de contenu Mon texte de contenu Mon texte de contenu Mon texte de contenu",
      id: 5,
      numberLikes: 10,
      numberDislikes: 10,
      autor: {},
      keyWords: ["retraite", "salaire"],
      idsFactChecking: [17],
      dateFormat: "02-01-2020",
      factCheckings: {
        For: [
          {
            autor: { lastName: "Ecran", surName: "Guillaume", id: 16 },
            url: "https://checkinformation.com",
            content:
              "Here you will find the proof that this argument is bullshit.1",
            title: "Mon titre"
          },
          {
            autor: {},
            url: "https://checkinformation.com",
            content:
              "Here you will find the proof that this argument is bullshit.2",
            title: "Mon titre"
          }
        ],
        Against: [
          {
            autor: { lastName: "Ecran", surName: "Guillaume", id: 16 },
            url: "https://checkinformation.com",
            content:
              "Here you will find the proof that this argument is bullshit.1",
            title: "Mon titre"
          },
          {
            autor: {},
            url: "https://checkinformation.com",
            content:
              "Here you will find the proof that this argument is bullshit.2",
            title: "Mon titre"
          }
        ]
      }
    }
  ],
  factCheckings: [
    {
      autor: { lastName: "Landeroin", surName: "Clement", id: 15 },
      url: "http://monsite.com",
      comment: "Sur ce site vous pouvez voir que...",
      id: 17
    }
  ],
  selectedDebate: {},
  user: {
    lastName: "Landeroin",
    surName: "Clement",
    id: 15,
    likesArgs: [6],
    dislikesArgs: [5],
    likesDebate: [0],
    dislikesDebate: [0],
    imgUrl:
      "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTF_erFD1SeUnxEpvFjzBCCDxLvf-wlh9ZuPMqi02qGnyyBtPWdE-3KoH3s",
    email: "myemail@voila.fr",
    login: "Clement123456789",
    description: "Ceci est la description de mon profile.",
    gender: "na"
  },
  imgUrl:
    "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTF_erFD1SeUnxEpvFjzBCCDxLvf-wlh9ZuPMqi02qGnyyBtPWdE-3KoH3s",
  email: "myemail@voila.fr",

  writingArg: NONE,
  isUserLog: false,
  profile: {
    lastName: "Ecran",
    surName: "Guillaume",
    id: 16,
    imgUrl:
      "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTF_erFD1SeUnxEpvFjzBCCDxLvf-wlh9ZuPMqi02qGnyyBtPWdE-3KoH3s",
    email: "myemail@voila.fr",
    login: "Clement123456789",
    description: "Ceci est la description de mon profile."
  },
  args: []
};
function reducer(state = initialState, action) {
  let newState = { ...state };
  switch (action.type) {
    case SET_DEBATES:
      newState.debates = action.payload;
      break;
    case SET_SELECTED_DEBATE:
      newState.selectedDebate = action.payload;
      break;
    case SET_DEBAT:
      newState.selectedDebate = action.payload;
      break;
    case SET_ARGSPOUR:
      newState.argsFor = action.payload;
      break;
    case SET_ARGSCONTRE:
      newState.argsAgainst = action.payload;
      break;
    case SET_USER:
      newState.user = action.payload;
      newState.isUserLog = true;
      break;
    case SET_ISUSERLOG:
      newState.user = {};
      newState.isUserLog = action.payload;
      break;
    case SET_FACTCHEKING:
      newState.factCheckings = action.payload;
      break;
    case SET_WRITING_MODE:
      newState.writingArg = action.payload;
      break;
    case UPDATE_LIKE:
      newState = action.payload;
      break;
    case SET_PROFILE:
      newState.profile = action.payload;
      break;
    case SET_FACT_CHECKING:
      newState.argsFor = action.payload.argsFor;
      newState.argsAgainst = action.payload.argsAgainst;
      break;
    case SET_ARGSUSER:
      newState.args = action.payload;
      break;
    default:
      return newState;
  }
  return newState;
}
export default reducer;
