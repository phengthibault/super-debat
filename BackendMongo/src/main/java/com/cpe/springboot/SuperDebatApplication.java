package com.cpe.springboot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Bean;
import org.springframework.context.event.EventListener;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.mongodb.Mongo;

@SpringBootApplication
public class SuperDebatApplication {

	private @Autowired Mongo mongo;
	private @Autowired MongoDbFactory mongoDbFactory;
	
	@Bean
	public PasswordEncoder passwordEncoder() {
	    return new BCryptPasswordEncoder();
	}

	/**
	 * Executed after application start
	 */
	@EventListener(ApplicationReadyEvent.class)
		//enable to be in topic mode! to do at start
	public void doInitAfterStartup() {
		mongo.dropDatabase(mongoDbFactory.getDb().getName());

	}

	public static void main(String[] args) {
		SpringApplication.run(SuperDebatApplication.class, args);


	}




}
