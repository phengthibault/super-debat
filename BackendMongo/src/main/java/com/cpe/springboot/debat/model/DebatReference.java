package com.cpe.springboot.debat.model;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.cpe.springboot.arg.model.ArgModel;
import com.cpe.springboot.user.model.UserShort;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Document(collection = "DebatReference")

@JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
public class DebatReference  implements Serializable  {


	/**
	 * 
	 */
	private static final long serialVersionUID = -7036383502151644217L;
	/**
	 * 
	 */
	@Id
//	@GeneratedValue(strategy = GenerationType.AUTO)
	private String id;
	private String title;
	private UserShort autor;
	private Integer numberLikes;
	private Integer numberDislikes;
	private HashMap<String, Integer> keywordFor=new HashMap<String, Integer>();;
	private HashMap<String, Integer> keywordAgainst=new HashMap<String, Integer>();;
	private ArgModel topArg;
	private String date;
	private String imgUrl;
	private String imgTitle;
	private String describe;
	private Integer percentageFor;
	private Integer percentageAgainst;

	public DebatReference(String title, UserShort autor, Integer numberLikes, Integer numberDislikes,
			HashMap<String, Integer> keywordFor, HashMap<String, Integer> keywordAgainst, ArgModel topArg, String date, String imgUrl, String imgTitle,
			String describe) {
		this.title = title;
		this.autor = autor;
		this.numberLikes = numberLikes;
		this.numberDislikes = numberDislikes;
		this.keywordFor = keywordFor;
		this.keywordAgainst = keywordAgainst;
		this.topArg = topArg;
		this.date = date;
		this.imgUrl = imgUrl;
		this.imgTitle = imgTitle;
		this.describe = describe;
		Integer numberLikesinInt=Integer.valueOf(numberLikes);
		Integer numberDislikesInt=Integer.valueOf(numberDislikes);
		Integer totalInteger=numberLikesinInt+numberDislikesInt;
		if (totalInteger>0) {
			this.percentageFor=numberLikesinInt*100/totalInteger;
			this.percentageAgainst= numberDislikesInt*100/totalInteger;
		}
		else {
			this.percentageFor=0;
			this.percentageAgainst=0;
		}
			
	}
	public DebatReference(String title, UserShort autor, String describe) {
		super();
		Calendar cal = Calendar.getInstance();
		java.util.Date date= cal.getTime();
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		String formattedDate=dateFormat.format(date);
		this.title = title;
		this.autor = autor;
		this.describe = describe;
		this.numberLikes = 0;
		this.numberDislikes = 0;
	
		this.topArg = new ArgModel();
		this.date = formattedDate;
		this.imgUrl = "";
		this.imgTitle = "";
		this.describe = describe;
		this.percentageFor=0;
		this.percentageAgainst=0;
	}
	public DebatReference(String title, UserShort autor, String describe,String imgUrl) {
		super();
		Calendar cal = Calendar.getInstance();
		java.util.Date date= cal.getTime();
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		String formattedDate=dateFormat.format(date);
		this.title = title;
		this.autor = autor;
		this.describe = describe;
		this.numberLikes = 0;
		this.numberDislikes = 0;
	
		this.topArg = new ArgModel();
		this.date = formattedDate;
		this.imgUrl = imgUrl;
		this.imgTitle = "";
		this.describe = describe;
		this.percentageFor=0;
		this.percentageAgainst=0;
	}
	public DebatReference() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}



	public Integer getNumberLikes() {
		return numberLikes;
	}

	public void setNumberLikes(Integer numberLikes) {
		this.numberLikes = numberLikes;
	}

	public Integer getNumberDislikes() {
		return numberDislikes;
	}

	public void setNumberDislikes(Integer numberDislikes) {
		this.numberDislikes = numberDislikes;
	}

	public HashMap<String, Integer> getKeywordFor() {
		return keywordFor;
	}

	public void setKeywordFor(HashMap<String, Integer> keywordFor) {
		this.keywordFor = keywordFor;
	}

	public HashMap<String, Integer> getKeywordAgainst() {
		return keywordAgainst;
	}

	public void setKeywordAgainst(HashMap<String, Integer> keywordAgainst) {
		this.keywordAgainst = keywordAgainst;
	}



	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public String getImgTitle() {
		return imgTitle;
	}

	public void setImgTitle(String imgTitle) {
		this.imgTitle = imgTitle;
	}

	public String getDescribe() {
		return describe;
	}

	public void setDescribe(String describe) {
		this.describe = describe;
	}

	public UserShort getAutor() {
		return autor;
	}

	public void setAutor(UserShort autor) {
		this.autor = autor;
	}

	public ArgModel getTopArg() {
		return topArg;
	}

	public void setTopArg(ArgModel topArgs) {
		this.topArg = topArgs;
	}

	public Integer getPercentageFor() {
		return percentageFor;
	}

	public void setPercentageFor(Integer percentageFor) {
		this.percentageFor = percentageFor;
	}

	public Integer getPercentageAgainst() {
		return percentageAgainst;
	}

	public void setPercentageAgainst(Integer percentageAgainst) {
		this.percentageAgainst = percentageAgainst;
	}
	public void updatePercentage() {
		
		Integer totalInteger=numberLikes+numberDislikes;
		Integer numberLikesinInt=Integer.valueOf(numberLikes);
		Integer numberDislikesInt=Integer.valueOf(numberDislikes);
		if (totalInteger>0) {
			this.percentageFor=numberLikesinInt*100/totalInteger;
			this.percentageAgainst= numberDislikesInt*100/totalInteger;
		}
		else {
			this.percentageFor=0;
			this.percentageAgainst=0;
		}
		
	}
	
}
