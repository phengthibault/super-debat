package com.cpe.springboot.debat.model;

import java.net.URL;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.cpe.springboot.arg.model.ArgModel;
import com.cpe.springboot.user.model.UserModel;
import com.cpe.springboot.user.model.UserShort;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Document(collection = "DebatModel")
@JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
public class DebatModel extends DebatReference implements Comparable<DebatModel>{
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3962172290322763518L;

	private Set<String> idArgsFor;

	private Set<String> idArgsAgainst;
	
	private Set<String> listIdLikers;
	private Set<String> listIdDisLikers;

	//private ArgModel argument;
	
	public DebatModel() {
		super();
	}
	public DebatModel(String title, UserShort autor, String describe) {
		super(title, autor, describe);
		this.idArgsFor =  Collections.<String>emptySet();
		this.idArgsAgainst =  Collections.<String>emptySet();
		this.listIdLikers = Collections.<String>emptySet();
		this.listIdDisLikers = Collections.<String>emptySet();
	}
	public DebatModel(String title, UserShort autor, String describe,String imgUrl) {
		super(title, autor, describe,imgUrl);
		this.idArgsFor =  Collections.<String>emptySet();
		this.idArgsAgainst =  Collections.<String>emptySet();
		this.listIdLikers = Collections.<String>emptySet();
		this.listIdDisLikers = Collections.<String>emptySet();
	}
	
	public DebatModel(String title, UserShort idCreator, Integer numberLikes, Integer numberDislikes, HashMap<String, Integer> keywordFor,
			HashMap<String, Integer> keywordAgainst, ArgModel idtopArg, String date, String imgUrl, String imgTitle, String describe,
			Set<String> argsFor, Set<String> argsAgainst, Set<String> listIdlikes, Set<String> listIdDislikes) {
		super(title, idCreator, numberLikes, numberDislikes, keywordFor, keywordAgainst, idtopArg, date, imgUrl,
				imgTitle, describe);
		this.idArgsFor = argsFor;
		this.idArgsAgainst = argsAgainst;
		this.listIdLikers = listIdlikes;
		this.listIdDisLikers = listIdDislikes;
	}
	public DebatModel(String title, UserShort idCreator, Integer numberLikes, Integer numberDislikes,HashMap<String, Integer> keywordFor,
			HashMap<String, Integer> keywordAgainst, ArgModel idtopArg, String dateFormat, String imgUrl, String imgTitle, String describe,
			Set<String> argsFor, Set<String> argsAgainst) {
		super(title, idCreator, numberLikes, numberDislikes, keywordFor, keywordAgainst, idtopArg, dateFormat, imgUrl,
				imgTitle, describe);
		this.idArgsFor = argsFor;
		this.idArgsAgainst = argsAgainst;
		this.listIdLikers = Collections.<String>emptySet();
		this.listIdDisLikers = Collections.<String>emptySet();
		}
	public DebatModel(String title, UserShort idCreator, Integer numberLikes, Integer numberDislikes, HashMap<String, Integer>keywordFor,
			HashMap<String, Integer>keywordAgainst, ArgModel idtopArg, String dateFormat, String imgUrl, String imgTitle, String describe) {
		super(title, idCreator, numberLikes, numberDislikes, keywordFor, keywordAgainst, idtopArg, dateFormat, imgUrl,
				imgTitle, describe);
		this.idArgsFor =  Collections.<String>emptySet();
		this.idArgsAgainst =  Collections.<String>emptySet();
		this.listIdLikers = Collections.<String>emptySet();
		this.listIdDisLikers = Collections.<String>emptySet();
	
		}
	public DebatModel(DebatReference debatRef) {
		super(debatRef.getTitle(), debatRef.getAutor(), debatRef.getDescribe(),debatRef.getImgUrl());
		
		this.idArgsFor =  Collections.<String>emptySet();
		this.idArgsAgainst =  Collections.<String>emptySet();
		this.listIdLikers = Collections.<String>emptySet();
		this.listIdDisLikers = Collections.<String>emptySet();
	}
	public Set<String> getIdArgsFor() {
		return idArgsFor;
	}
	public void setIdArgsFor( Set<String>argsFor) {
		this.idArgsFor = argsFor;
	}
	public  Set<String> getIdArgsAgainst() {
		return idArgsAgainst;
	}
	public void setIdArgsAgainst( Set<String> argsAgainst) {
		this.idArgsAgainst = argsAgainst;
	}
	public Set<String> getListIdLikers() {
		return listIdLikers;
	}
	public void setListIdLikers(Set<String> listIdLikers) {
		this.listIdLikers = listIdLikers;
	}
	public Set<String> getListIdDisLikers() {
		return listIdDisLikers;
	}
	public void setListIdDisLikers(Set<String> listIdDisLikers) {
		this.listIdDisLikers = listIdDisLikers;
	}
	@Override
	public int compareTo(DebatModel o) {
		return getDate().compareTo(o.getDate());
	}



	

	
}
