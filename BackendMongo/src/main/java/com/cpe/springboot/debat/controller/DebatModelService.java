package com.cpe.springboot.debat.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import com.cpe.springboot.arg.FactCheck.FactCheckModel;
import com.cpe.springboot.arg.controller.ArgModelRepository;
import com.cpe.springboot.arg.model.ArgModel;
import com.cpe.springboot.debat.model.DebatModel;
import com.cpe.springboot.user.controller.UserRepository;
import com.cpe.springboot.user.controller.UserService;
import com.cpe.springboot.user.model.UserModel;
import com.cpe.springboot.user.model.UserShort;

@Service
public class DebatModelService {


	public DebatModelService() {
	}


	@Autowired
	private DebatModelRepository DebatRepository;
	@Autowired
	private ArgModelRepository argModelRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private UserService userService;


	public List<DebatModel> getAllDebatModel() {
		List<DebatModel> DebatList = new ArrayList<>();
		DebatRepository.findAll().forEach(DebatList::add);
		return DebatList;
	}
	public List<DebatModel> getAllDebatRecent() {
		List<DebatModel> DebatList = new ArrayList<>();
		DebatRepository.findAll().forEach(DebatList::add);
		//		Collections.sort(DebatList,Collections.reverseOrder());
		Collections.sort(DebatList);
		DebatList.forEach(action-> System.out.println(action.getDate()));
		return DebatList;
	}
	public void addDebat(DebatModel DebatModel) {
		DebatRepository.save(DebatModel);
	}

	public void updateDebatRef(DebatModel DebatModel) {
		DebatRepository.save(DebatModel);


	}
	public void updateDebat(DebatModel DebatModel) {
		DebatRepository.save(DebatModel);
	}
	public Optional<DebatModel> getDebat(String id) {
		return DebatRepository.findById(id);
	}

	public void deleteDebatModel(String id) {
		DebatRepository.deleteById(id);
	}


	/**
	 * Executed after application start
	 */

	@EventListener(ApplicationReadyEvent.class)
	public void doInitAfterStartup() {
		Calendar cal = Calendar.getInstance();
		java.util.Date date= cal.getTime();
		DateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
		String formattedDate=dateFormat.format(date);

		ArgModel comment,commentpos1,commentneg1,commentNeg,commentpos3;


		DebatModel debatModeltest,debatModeltest2,debatModel3;


		UserModel userModel =new UserModel("Anonymous","Anonymous","machin@email.com","https://akns-images.eonline.com/eol_images/Entire_Site/2019414/rs_634x1024-190514063109-634-George-Clooney-Batman-LT-051419-GettyImages-607408286.jpg?fit=inside|900:auto&output-quality=90");
		userService.addUser(userModel);
		UserModel user2 =new UserModel("tp", "tp", "ecran", "guillaume", "email@voila.fr", "https://www.francetvinfo.fr/image/759r5hedj-cf2b/580/326/10410843.jpg");
		userService.addUser(user2);
		UserShort userShort2=new UserShort(user2.getId(),user2.getLastName(), user2.getSurName());
		UserShort userShort1=new UserShort(userModel.getId(),userModel.getLastName(), userModel.getSurName());
		FactCheckModel factCheckModel=new FactCheckModel();
		List<String> list= new ArrayList<String>();list.add("1");
		FactCheckModel factCheckModel2=new FactCheckModel(userShort2, "testurl", "oui",list) ;
		HashMap<String, List<FactCheckModel>> factCheckModels=new HashMap<String, List<FactCheckModel>>();

		List<FactCheckModel> fList=new ArrayList<FactCheckModel>();
		fList.add(factCheckModel2);
		factCheckModels.put("For", fList);
		factCheckModels.put("Against", fList);

		 HashMap<String, String> keyWords=new HashMap<String, String>();
		 keyWords.put("retirement", "0.1");
		commentpos1=new ArgModel("Yes, the retirement age should be raised to take the pressure off social security and the budget deficit. The Social Security system is in disarray, and it is threatening to go bankrupt. An increase in the retirement age, and the associated age at which Social Security payments begin, would take the pressure off the spiraling budget deficit. As medical care has gotten better, people are living longer, so an increase in retirement is appropriate because people can be productive workers longer."
				,keyWords,3,4,userShort1,formattedDate,factCheckModels,true,"1");

		commentneg1=new ArgModel("Work till you drop No no no working after 65 is madness the Manuel worker such as I Can not just keep going on and on eyes fail as well as body joints and memory for gods sake we no it's a money grabbing scheme we are not daft the government just imposes it we follow (sheep) enough said ."
				,keyWords,1,2,userShort2,formattedDate,factCheckModels,false,"2");

		commentpos3=new ArgModel("Yes it will raise",keyWords,1,2,userShort2,formattedDate,factCheckModels,false,"2");
		HashMap<String, Integer> keyworforStrings=new HashMap<String, Integer>();
		HashMap<String, Integer> keyworforStrings3=new HashMap<String, Integer>();
		keyworforStrings3.put("bitcoin", 3);
		HashMap<String, Integer> keyworforStrings3bis=new HashMap<String, Integer>();
		keyworforStrings3bis.put("argent", 3);
		HashMap<String, Integer> keyworAgainstStrings=new HashMap<String, Integer>();
		keyworforStrings.put("retraite", 3);
		keyworforStrings.put("age", 1);
		keyworAgainstStrings.put("vieux", 1);
		String imgUrlString="https://static.lexpress.fr/medias_12190/w_2048,h_1146,c_crop,x_0,y_215/w_480,h_270,c_fill,g_north/v1578081234/manifestation-a-paris-contre-la-reforme-des-retraites-le-28-decembre-2019-1_6241738.jpg";
		String imgUrlString2="https://file1.pleinevie.fr/var/pleinevie/storage/images/1/7/1/171889/retraites-les-axes-reforme.jpg?alias=width400&size=x100&format=jpeg";
		String imgUrlString3="https://specials-images.forbesimg.com/imageserve/5e00ed59da9b5c0007eb9ac7/960x0.jpg?fit=scale";

		String description="Le système de retraite français fonctionne comme une assurance collective. Les travailleurs (et les employeurs) financent les caisses de retraite en s’acquittant de cotisations prélevées sur leurs revenus. Ces sommes servent ensuite à payer les pensions de retraite.";
		String description2="Clément veut sa retraite isses de retraite en s’acquittant de cotisations prélevées sur leurs revenus. Ces sommes servent ensuite à payer les pensions de retraite.";
		String description3="Will Bitcoin Crash or Rise?";

		debatModeltest=new DebatModel("retraite1", userShort2,35, 6, keyworAgainstStrings,keyworAgainstStrings, commentpos1,formattedDate,imgUrlString, "imgTitle",description) ;//listeArgForArgModels,listeArgAgainst
		debatModeltest2=new DebatModel("retraite2", userShort2,3, 4, keyworforStrings,keyworforStrings, commentpos1,formattedDate,imgUrlString2, "imgTitle",description2) ;//listeArgForArgModels,listeArgAgainst
		
		debatModel3=new DebatModel("Bitcoin", userShort2,3, 4, keyworforStrings3,keyworforStrings3bis, commentpos3,formattedDate,imgUrlString3, "Bitcoin",description3) ;
		DebatRepository.save(debatModeltest);
		DebatRepository.save(debatModeltest2);
		DebatRepository.save(debatModel3);

		String iddebat=DebatRepository.findAll().get(0).getId();


		//comment= new ArgModel("mon commentaire positif dans la BD", keyWords, 2, 14,userShort2,formattedDate,factCheckModels,true,iddebat);
		commentNeg= new ArgModel("mon commentaire negatif", keyWords, 2, 14,userShort1,formattedDate,factCheckModels,false,iddebat);
	//	argModelRepository.save(comment);
		argModelRepository.save(commentpos1);
		argModelRepository.save(commentNeg);
	//	argModelRepository.save(comment);

		argModelRepository.save(commentpos3);
		Set<String>listeArgForArgModels=  new HashSet<>();
	//	listeArgForArgModels.add(comment.getId());
		listeArgForArgModels.add(commentpos1.getId());
		Set<String>listeArgForArgModels3=  new HashSet<>();
		listeArgForArgModels3.add(commentpos3.getId());
		Set<String>listeArgAgainst=  new HashSet<>();
		listeArgAgainst.add(commentNeg.getId());
		
		debatModeltest.setIdArgsAgainst(listeArgAgainst);
		debatModeltest.setIdArgsFor(listeArgForArgModels);
		debatModeltest2.setIdArgsAgainst(listeArgAgainst);
		debatModeltest2.setIdArgsFor(listeArgForArgModels);
		debatModel3.setIdArgsFor(listeArgForArgModels3);
		DebatRepository.save(debatModel3);

		DebatRepository.save(debatModeltest);
		DebatRepository.save(debatModeltest2);


	}

	public List<DebatModel> getTopDebatModel() {
		List<DebatModel> DebatList = new ArrayList<>();
		DebatRepository.findAll().forEach(DebatList::add);
		Comparator<DebatModel> compareByLikes = (DebatModel o1, DebatModel o2) -> o1.getNumberLikes().compareTo( o2.getNumberLikes() );
		Collections.sort(DebatList,compareByLikes);
		return DebatList;
	}



}
