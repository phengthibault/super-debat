package com.cpe.springboot.debat.controller;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.CrudRepository;

import com.cpe.springboot.debat.model.DebatModel;
import com.cpe.springboot.debat.model.DebatReference;

public interface DebatRefRepository extends MongoRepository<DebatReference, String> {
    Optional<DebatReference> findById(String id);

}
