package com.cpe.springboot.debat.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cpe.springboot.arg.controller.ArgModelService;
import com.cpe.springboot.arg.model.ArgModel;
import com.cpe.springboot.debat.model.DebatModel;
import com.cpe.springboot.debat.model.DebatReference;
import com.cpe.springboot.user.controller.UserService;
import com.cpe.springboot.user.model.UserModel;

@CrossOrigin
@RestController
public class DebatRestController {


	@Autowired
	private DebatModelService debatModelService;
	@Autowired
	private DebatRefService debatRefService;
	@Autowired
	private ArgModelService argModelService;
	@Autowired
	private UserService userService;
	@RequestMapping("/Debats")
	private List<DebatModel> getAllDebats() {
		return debatModelService.getAllDebatModel();
		//return debatRefService.getAllDebatRef();
	}

	@RequestMapping("/Debats/keyWords")
	private Set<String> getAllDebatsKeywords() {
		List<DebatModel> dModels=debatModelService.getAllDebatModel();
		Set<String> listKeywords=new HashSet<String>();
		for(DebatModel dModel:dModels) {
			for(String key:	dModel.getKeywordFor().keySet()) {
				listKeywords.add(key);
			}
			for(String key:	dModel.getKeywordAgainst().keySet()) {
				listKeywords.add(key);
			}
		}
		return listKeywords;
	}

	@RequestMapping("/getDebats/{keywords}")
	private Set<DebatModel> getDebatsWithKeywords(@PathVariable String keywords) {
		Set<DebatModel> ldebatContainingKeywords=new HashSet<DebatModel>();
		List<DebatModel> dModels=debatModelService.getAllDebatModel();
		for(DebatModel dModel:dModels) {
			for(String key:	dModel.getKeywordFor().keySet()) {
				if (key.equals(keywords)) {
					ldebatContainingKeywords.add(dModel);
				}
			}
			for(String key:	dModel.getKeywordAgainst().keySet()) {
				if (key.equals(keywords)) {
					ldebatContainingKeywords.add(dModel);
				}					}
		}
		return ldebatContainingKeywords;
	}




	@RequestMapping("/Debats/Title")
	private List<String> getAllDebatsTitle() {
		List<DebatModel> lDebatModels= debatModelService.getAllDebatModel();
		List<String> lTitle=new ArrayList<String>();
		for (DebatModel debatModel:lDebatModels) {
			lTitle.add(debatModel.getTitle());
		}
		return lTitle;
	}

	@RequestMapping("/DebatsRecent")
	private List<DebatModel> DebatsRecent() {
		return debatModelService.getAllDebatRecent();
	}

	@RequestMapping("/Debat/{id}")
	private Optional<DebatModel> getDebat(@PathVariable String id) {
		Optional<DebatModel> rDebat;
		rDebat= debatModelService.getDebat(id);
		return rDebat;

	}

	@RequestMapping("/TopDebats")
	private List<DebatModel> getTopDebat() {
		return debatModelService.getTopDebatModel();
	}


	private List<ArgModel> getDebatListArgsFor(String id,String type) {
		Optional<DebatModel> rDebat;		
		List<ArgModel> listArgFor = new ArrayList<ArgModel>();
		Set<String> listidArgFor=new HashSet<String>();
		rDebat= debatModelService.getDebat(id);
		if(rDebat.isPresent()) {
			DebatModel debatModel=rDebat.get();
			listidArgFor=debatModel.getIdArgsFor();
			System.out.print(listidArgFor);

			for (String idArgFor:listidArgFor) {
				listArgFor.add(argModelService.getArg(idArgFor).get());
			}
			if (type=="HOT") {
				Comparator<ArgModel> compareByLikes = (ArgModel o1, ArgModel o2) -> o2.getNumberLikes().compareTo( o1.getNumberLikes() );
				Collections.sort(listArgFor,compareByLikes);

				//Collections.sort(listArgFor,Collections.reverseOrder());
			}
			if (type=="FRESH") {
				Collections.sort(listArgFor);
				Collections.sort(listArgFor,Collections.reverseOrder());

			}
		}
		return listArgFor;

	}


	@RequestMapping("/DebatArgsFor/{id}")
	private List<ArgModel> getDebatArgsFor(@PathVariable String id) {

		return getDebatListArgsFor(id,"") ;
	}
	@RequestMapping("/SortedArgsForHOT/{Debatid}")
	private List<ArgModel> getDebatArgsForByLykes(@PathVariable String Debatid) {

		return getDebatListArgsFor(Debatid,"HOT");
	}

	@RequestMapping("/SortedArgsForFRESH/{Debatid}")
	private List<ArgModel> getDebatArgsForByDate(@PathVariable String Debatid) {

		return getDebatListArgsFor(Debatid,"FRESH");
	}

	private List<ArgModel> getDebatListArgsAgainst(String id,String type) {
		Optional<DebatModel> rDebat;		
		List<ArgModel> listArgAgainst= new ArrayList<ArgModel>();
		Set<String>  listidArgFor=new HashSet<String>();

		rDebat= debatModelService.getDebat(id);
		if(rDebat.isPresent()) {
			DebatModel debatModel=rDebat.get();
			listidArgFor=debatModel.getIdArgsAgainst();

			for (String idArgAgainst:listidArgFor) {
				listArgAgainst.add(argModelService.getArg(idArgAgainst).get());
			}
			if (type=="HOT") {
				Comparator<ArgModel> compareByLikes = (ArgModel o1, ArgModel o2) -> o2.getNumberLikes().compareTo( o1.getNumberLikes() );
				Collections.sort(listArgAgainst,compareByLikes);
				//Collections.sort(listArgFor,Collections.reverseOrder());
			}
			if (type=="FRESH") {
				Collections.sort(listArgAgainst);
				Collections.sort(listArgAgainst,Collections.reverseOrder());
			}
		}
		return listArgAgainst;
	}


	@RequestMapping("/DebatArgsAgainst/{id}")
	private List<ArgModel> getDebatArgsAgainst(@PathVariable String id) {
		return getDebatListArgsAgainst(id,"");
	}


	@RequestMapping("/SortedArgsAgainstHOT/{Debatid}")
	private List<ArgModel> getDebatArgsAgainstByLykes(@PathVariable String Debatid) {

		return getDebatListArgsAgainst(Debatid,"HOT");
	}

	@RequestMapping("/SortedArgsAgainstFRESH/{Debatid}")
	private List<ArgModel> getDebatArgsAgainstByDate(@PathVariable String Debatid) {
		return getDebatListArgsAgainst(Debatid,"FRESH");
	}


	@RequestMapping(method=RequestMethod.POST,value="/Debat")
	public boolean addDebat(@RequestBody DebatReference debatRef) {
		DebatModel debatModel=new DebatModel(debatRef);
		debatModelService.addDebat(debatModel);
		return true;
	}



	@RequestMapping(method=RequestMethod.PUT,value="/Debat/{id}")
	public void updateDebat(@RequestBody DebatModel Debat,@PathVariable String id) {
		Debat.setId(id);
		debatModelService.updateDebat(Debat);
	}



	@RequestMapping(method=RequestMethod.DELETE,value="/Debat/{id}")
	public void deleteUser(@PathVariable String id) {
		debatModelService.deleteDebatModel(id);
	}


	@RequestMapping(method=RequestMethod.GET,value="/DebatUpdateLike/{idDebat}/{idLiker}")
	public boolean updateDebatLikes(@PathVariable String idDebat,@PathVariable String idLiker) {

		Optional<DebatModel> debatModel=debatModelService.getDebat(idDebat);
		if (!debatModel.isPresent()) {
			return false;
		}		
		DebatModel debat=debatModel.get();
		UserModel userModel=userService.getUser(idLiker).get();

		if (debat.getListIdLikers().add(idLiker)) {  //check if not already like by the user otherwise remove the like
			userModel.getLikesDebate().add(idDebat);
			userModel.setLikesDebate(userModel.getLikesDebate());
			debat.setListIdLikers(debat.getListIdLikers());		
			debat.setNumberLikes(debat.getNumberLikes()+1);
		}
		else {
			debat.getListIdLikers().remove(idLiker);
			debat.getListIdLikers().remove(idLiker);
			debat.setNumberLikes(debat.getNumberLikes()-1);
		}		
		debat.updatePercentage();

		userService.updateUser(userModel);
		debatModelService.updateDebat(debat);
		return true;
	}

	@RequestMapping(method=RequestMethod.GET,value="/DebatUpdateDisLike/{idDebat}/{idLiker}")
	public boolean updateDebatDisLikes(@PathVariable String idDebat,@PathVariable String idLiker) {

		Optional<DebatModel> debatModel=debatModelService.getDebat(idDebat);
		if (!debatModel.isPresent()) {
			return false;
		}		
		DebatModel debat=debatModel.get();
		UserModel userModel=userService.getUser(idLiker).get();
		if (debat.getListIdDisLikers().add(idLiker)) {  //check if not already like by the user otherwise remove the like
			userModel.getDislikesDebate().add(idDebat);
			userModel.setDislikesDebate(userModel.getDislikesDebate());
			debat.setListIdDisLikers(debat.getListIdDisLikers());		
			debat.setNumberDislikes(debat.getNumberDislikes()+1);
		}
		else {
			debat.getListIdDisLikers().remove(idLiker);
			debat.getListIdDisLikers().remove(idLiker);
			debat.setNumberDislikes(debat.getNumberDislikes()-1);
		}
		debat.updatePercentage();
		userService.updateUser(userModel);
		debatModelService.updateDebat(debat);
		return true;

	}


}
