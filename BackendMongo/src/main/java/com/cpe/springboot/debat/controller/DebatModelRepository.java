package com.cpe.springboot.debat.controller;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.CrudRepository;

import com.cpe.springboot.debat.model.DebatModel;


public interface DebatModelRepository extends MongoRepository<DebatModel, String> {
    Optional<DebatModel> findById(String id);
}
