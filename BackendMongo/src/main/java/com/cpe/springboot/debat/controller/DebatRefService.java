package com.cpe.springboot.debat.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import com.cpe.springboot.arg.model.ArgModel;
import com.cpe.springboot.debat.model.DebatReference;
@Service
public class DebatRefService {
	
	public DebatRefService() {
	}


	@Autowired
	private DebatRefRepository DebatRefRepository;
	


	public List<DebatReference> getAllDebatRef() {
		List<DebatReference> DebatList = new ArrayList<>();
		DebatRefRepository.findAll().forEach(DebatList::add);
		return DebatList;
	}

	public void addDebat(DebatReference DebatRef) {
		DebatRefRepository.save(DebatRef);
	}

	public void updateDebatRef(DebatReference DebatRef) {
		DebatRefRepository.save(DebatRef);

	}
	public void updateDebat(DebatReference DebatRef) {
		DebatRefRepository.save(DebatRef);
	}
	public Optional<DebatReference> getDebat(String id) {
		return DebatRefRepository.findById(id);
	}
	
	public void deleteDebatRef(String id) {
		DebatRefRepository.deleteById(id);
	}
	
	
	/**
	 * Executed after application start
	 */
	

}
