package com.cpe.springboot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.event.EventListener;
import org.springframework.http.HttpEntity;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;


//https://attacomsian.com/blog/http-requests-resttemplate-spring-boot

@Service
public class RestService {

	private final RestTemplate restTemplate;

	public RestService(RestTemplateBuilder restTemplateBuilder) {
		this.restTemplate = restTemplateBuilder.build();
	}

	/**
	 * GET Request 
	 * @return
	 */
	public String getPostsPlainJSON() {
		String url = "http://127.0.0.1:5000/quarks";
		return this.restTemplate.getForObject(url, String.class);
	}
	

	public String getKeyWostPostsText(String data) {

		//String data1 = "/public/";
		org.springframework.http.HttpHeaders headers = new org.springframework.http.HttpHeaders();
		headers.setContentType(org.springframework.http.MediaType.TEXT_PLAIN);

		HttpEntity<String> request = new HttpEntity<String>(
				data, headers);
		
		String url = "http://127.0.0.1:5000/hello";
		
		restTemplate.getMessageConverters().add(new FormHttpMessageConverter());
		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
		String response = restTemplate
				.postForObject(url, request, String.class);
		return response;

	}
	
	
	@EventListener(ApplicationReadyEvent.class)
	public void doInitAfterStartup() {
		String data = "test";
		//data=getPostsPlainJSON();
		//System.out.println(data);
//	String res=getKeyWostPostsText(data);
//	System.out.println(res);
	}
	

}
