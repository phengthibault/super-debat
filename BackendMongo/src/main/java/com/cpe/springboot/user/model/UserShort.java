package com.cpe.springboot.user.model;



public class UserShort {
	private String id;
	private String lastName;
	private String surName;
	
	public UserShort() {
		
	}
	public UserShort(String id,String lastName, String surName ) {
		super();
		this.lastName = lastName;
		this.surName = surName;
		this.id = id;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getSurName() {
		return surName;
	}
	public void setSurName(String surName) {
		this.surName = surName;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

}
