package com.cpe.springboot.user.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Generated;


import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

//@Entity
@Document(collection = "UserModel")
//@JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
public class UserModel implements Serializable {
	
		private static final long serialVersionUID = 2733795832476568049L;
	@org.springframework.data.annotation.Id
	private String id;
	private String login;
	private String pwd;
	private String lastName;
	private String surName;
	private String email;
	private String imgUrl;
	private List<String> likesArgs=new ArrayList<String>();;
	private List<String> dislikesArgs=new ArrayList<String>();;
	private List<String> likesDebate=new ArrayList<String>();;
	private List<String> dislikesDebate=new ArrayList<String>();;
	
	//@OneToMany(cascade = CascadeType.ALL,
	//		mappedBy = "user")
	//@OneToMany(mappedBy = "user")
	//private Set<CardModel> cardList = new HashSet<>();
	
	public UserModel(String login, String pwd, String lastName, String surName, String email, String imgUrl) {
		this.login = login;
		this.pwd = pwd;
		this.lastName = lastName;
		this.surName = surName;
		this.email = email;
		this.imgUrl = imgUrl;
	/*	this.likesArgs=new ArrayList<String>();
		this.dislikesArgs=new ArrayList<String>();
		this.likesDebate=new ArrayList<String>();
		this.dislikesDebate=new ArrayList<String>();	
		this.likesArgs.add("0");
		this.dislikesArgs.add("0");
		this.likesDebate.add("0");
		this.dislikesDebate.add("0");*/

	}

	
	public UserModel(String login, String pwd, String lastName, String surName, String email, String imgUrl,
			List<String> likesArgs, List<String> dislikesArgs, List<String> likesDebate, List<String> dislikesDebate) {
		super();
		this.login = login;
		this.pwd = pwd;
		this.lastName = lastName;
		this.surName = surName;
		this.email = email;
		this.imgUrl = imgUrl;
		/*this.likesArgs = likesArgs;
		this.dislikesArgs = dislikesArgs;
		this.likesDebate = likesDebate;
		this.dislikesDebate = dislikesDebate;*/
	}


	public UserModel(String login, String pwd, String email) {
		this.login = login;
		this.pwd = pwd;
		this.email = email;
		this.lastName="Anonyme";
		this.surName="Anonyme";
		/*this.likesArgs=new ArrayList<String>();
		this.dislikesArgs=new ArrayList<String>();
		this.likesDebate=new ArrayList<String>();
		this.dislikesDebate=new ArrayList<String>();	
		this.likesArgs.add("0");
		this.dislikesArgs.add("0");
		this.likesDebate.add("0");
		this.dislikesDebate.add("0");*/
	}
	public UserModel(String login, String pwd, String email,String imgUrl) {
		this.login = login;
		this.pwd = pwd;
		this.email = email;
		this.imgUrl=imgUrl;
		this.lastName="Anonyme";
		this.surName="Anonyme";
		/*this.likesArgs=new ArrayList<String>();
		this.dislikesArgs=new ArrayList<String>();
		this.likesDebate=new ArrayList<String>();
		this.dislikesDebate=new ArrayList<String>();	
		this.likesArgs.add("0");
		this.dislikesArgs.add("0");
		this.likesDebate.add("0");
		this.dislikesDebate.add("0");*/
	}
	
	
	public UserModel() {
		this.login = "";
		this.pwd = "";
		this.lastName="lastname_default";
		this.surName="surname_default";
		this.email="email_default";
		this.imgUrl="default";
	}

	public UserModel(String login, String pwd) {
		this.login = login;
		this.pwd = pwd;
		this.lastName="Anonyme";
		this.surName="Anonyme";
	}



	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}



	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getSurName() {
		return surName;
	}
	public void setSurName(String surName) {
		this.surName = surName;
	}
	public String getImgUrl() {
		return imgUrl;
	}
	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public List<String> getLikesArgs() {
		return likesArgs;
	}


	public void setLikesArgs(List<String> listIdAgsLikes) {
		this.likesArgs = listIdAgsLikes;
	}


	public List<String> getDislikesArgs() {
		return dislikesArgs;
	}


	public void setDislikesArgs(List<String> listIdAgsDisLikes) {
		this.dislikesArgs = listIdAgsDisLikes;
	}


	public List<String> getLikesDebate() {
		return likesDebate;
	}


	public void setLikesDebate(List<String> likesDebate) {
		this.likesDebate = likesDebate;
	}


	public List<String> getDislikesDebate() {
		return dislikesDebate;
	}


	public void setDislikesDebate(List<String> dislikesDebate) {
		this.dislikesDebate = dislikesDebate;
	}


	
}
