package com.cpe.springboot.user.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.CrudRepository;

import com.cpe.springboot.user.model.UserModel;

public interface UserRepository extends MongoRepository<UserModel, String> {
	
	List<UserModel> findByLoginAndPwd(String login,String pwd);
	List<UserModel> findByLogin(String login);
	Optional<UserModel> findByEmail(String email);
	Optional<UserModel> findBylastName(String lastName);

}
