package com.cpe.springboot.user.controller;

import java.util.Enumeration;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.websocket.Session;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.session.SessionRepository;
import org.springframework.session.data.mongo.MongoOperationsSessionRepository;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cpe.springboot.HttpSessionConfig;
import com.cpe.springboot.user.model.User;
import com.cpe.springboot.user.model.UserModel;

//ONLY FOR TEST NEED ALSO TO ALLOW CROOS ORIGIN ON WEB BROWSER SIDE
@CrossOrigin
@RestController
public class UserRestController {
	
	@Autowired
	private UserService userService;
	@Autowired

	private SessionRepository SessionRepository;
	@Autowired
	private PasswordEncoder passwordEncoder;

	
	  @Autowired
	    private MongoOperationsSessionRepository repository;
    @RequestMapping("/test/{idUser}")
    public ResponseEntity<Integer> count(HttpSession session,@PathVariable String idUser) {
    	UUID uuid = UUID.randomUUID();
        String randomUUIDString = uuid.toString();
        System.out.println(randomUUIDString);

        session.setAttribute(idUser, randomUUIDString);
         System.out.println(session.getAttribute(idUser));
         
        return ResponseEntity.ok(1);
    }
    @RequestMapping("/userConnected/{idSession}")
    public UserModel getIsuserConnected( HttpServletRequest request,@PathVariable String idSession) {
    	System.out.println("idSession recu"+idSession);
//    	String	userId2= (String) request.getAttribute(idSession);
    	String	userId;
    	try {
    		userId= repository.findById(idSession).getAttribute(idSession);
        	//String	idSessioninDB= (String) session.getAttribute(idSession);
            System.out.println("userId"+userId);
    		return userService.getUser(userId).get();

		} catch (Exception e) {
            System.out.println(e.getMessage());
			return null;
		}
    }
    
    
    
	@RequestMapping("/users")
	private List<UserModel> getAllUsers() {
		return userService.getAllUsers();

	}
	
	@RequestMapping("/user/{id}")
	private UserModel getUser(@PathVariable String id) {
		Optional<UserModel> ruser;
		ruser= userService.getUser(id);
		if(ruser.isPresent()) {
			return ruser.get();
		}
		System.out.println("user not find");
		return null;

	}
	
	@RequestMapping(method=RequestMethod.POST,value="/user")
	public String addUser(@RequestBody UserModel user) {
		Optional<UserModel>  ruser;
		ruser= userService.getUserByEmail(user.getEmail());
		if(ruser.isPresent()) {
			return "email already used";
		}
		ruser= userService.getUserByLastName(user.getLastName());
		if(ruser.isPresent()) {
			return "email already used";
		}
		
		userService.addUser(user);
		return "user add :"+ user.getLastName()+" "+user.getSurName();
	}
	
	@RequestMapping(method=RequestMethod.PUT,value="/user/{id}/{oldPwd}")
	public String updateUser(@RequestBody UserModel user,@PathVariable String id,@PathVariable String oldPwd) {
		String login=user.getLogin();
		if (userService.getUserByLoginPwd(login,oldPwd)==null) {
			return "Wrong Password";
					
		}else {		
			user.setId(id);
			System.out.println(id);
			user.setPwd(passwordEncoder.encode(user.getPwd()));
			userService.updateUser(user);
			return "true";
		}
	}
	
	@RequestMapping(method=RequestMethod.DELETE,value="/user/{id}")
	public void deleteUser(@PathVariable String id) {
		userService.deleteUser(id);
	}

	

	
	@RequestMapping(method=RequestMethod.POST,value="/auth",consumes="application/json")
	private  ResponseEntity<UserSession>  getAuthentification( @RequestBody User userBody,HttpSession session) {
		String login=userBody.getLogin();
		String pwd=userBody.getPwd();
			System.out.println("login:"+login +" pwd "+ pwd);
		if( userService.getUserByLoginPwd(login,pwd)!=null) {
			UserModel user=userService.getUserByLoginPwd(login,pwd);
			System.out.println(user.getEmail());
			//UUID uuid = UUID.randomUUID();
	        //String sessionID = uuid.toString();
			String sessionID =session.getId();

	        System.out.println("sessionid"+sessionID);
	        
	        session.setAttribute(sessionID,user.getId());
	        UserSession userSession=new UserSession(user.getId(), sessionID);
			return  new ResponseEntity<>(userSession, HttpStatus.OK);
		}
		return  new ResponseEntity<>(null, HttpStatus.OK);
	}
	
    
	/*
	@RequestMapping(method=RequestMethod.POST,value="/auth",consumes="application/json")
	private  ResponseEntity<String>  getAllCourses( @RequestBody User userBody) {
		String login=userBody.getLogin();
		String pwd=userBody.getPwd();
			System.out.println("login:"+login +" pwd "+ pwd);
		if( userService.getUserByLoginPwd(login,pwd)!=null) {
			UserModel user=userService.getUserByLoginPwd(login,pwd);
			System.out.println(user.getEmail());
			return  new ResponseEntity<>(user.getId(), HttpStatus.OK);
		}
		return  new ResponseEntity<>(null, HttpStatus.OK);
	}*/
	
	

}
