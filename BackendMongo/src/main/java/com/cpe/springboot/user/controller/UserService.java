package com.cpe.springboot.user.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.cpe.springboot.arg.controller.ArgModelRepository;
import com.cpe.springboot.arg.controller.ArgModelService;
//import com.cpe.springboot.arg.controller.ArgModelRepository;
//import com.cpe.springboot.arg.controller.ArgModelService;
import com.cpe.springboot.user.model.UserModel;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private ArgModelRepository argModelrepository;
	
	@Autowired
	private ArgModelService argModelService;

	@Autowired
	private PasswordEncoder passwordEncoder;

	public List<UserModel> getAllUsers() {
		List<UserModel> userList = new ArrayList<>();
		userRepository.findAll().forEach(userList::add);
		return userList;
	}


	public Optional<UserModel> getUser(String id) {
		return userRepository.findById(id);
	}

	
	public void addUser(UserModel user) {
		// encode password 
		user.setPwd(passwordEncoder.encode(user.getPwd()));
		userRepository.save(user);

	}

	public void updateUser(UserModel user) {
		userRepository.save(user);

	}

	public void deleteUser(String id) {
		userRepository.deleteById(id);
	}

	public UserModel getUserByLoginPwd(String login, String pwd) {
		UserModel ufind=null;
		UserModel userModel = null;
		List<UserModel> ulist= userRepository.findByLogin(login);
		if (ulist.isEmpty()) {
			System.out.println("user non trouvé");
		}else {
		
		  userModel=ulist.get(0);
		}
		System.out.println("le password est trouvé ?"+ passwordEncoder.matches(pwd,userModel.getPwd()));
		if (passwordEncoder.matches(pwd,userModel.getPwd())){
			ufind=userModel;
		}
		return ufind;
	}
	public List<UserModel> getUserByLogin(String login) {
		List<UserModel> ulist=null;
		ulist=userRepository.findByLogin(login);
		return ulist;
	}
	
	/**
	 * Executed after application start
	 */
	//@EventListener(ApplicationReadyEvent.class)
	public void doInitAfterStartup() {
//		for(int i=0;i<10;i++){
			UserModel userModel =new UserModel("Anonymous","Anonymous","machin@email.com");
			addUser(userModel);
			UserModel user2 =new UserModel("tp", "tp", "john", "doe", "email@voila.fr", "https://www.francetvinfo.fr/image/759r5hedj-cf2b/580/326/10410843.jpg");
			addUser(user2);


	//	}
	
	}


	public Optional<UserModel> getUserByEmail(String email) {
	
		return userRepository.findByEmail(email);
	}


	public Optional<UserModel> getUserByLastName(String lastName) {
		return userRepository.findBylastName(lastName);
	}


	
}
