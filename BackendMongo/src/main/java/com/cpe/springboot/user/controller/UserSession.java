package com.cpe.springboot.user.controller;





public class UserSession {
	

private String id;
private String sessionID;

public UserSession(String userID, String sessionID) {
		super();
		this.id = userID;
		this.sessionID = sessionID;
	}
public String getId() {
	return id;
}
public void setId(String userID) {
	this.id = userID;
}
public String getSessionID() {
	return sessionID;
}
public void setSessionID(String sessionID) {
	this.sessionID = sessionID;
}
}
