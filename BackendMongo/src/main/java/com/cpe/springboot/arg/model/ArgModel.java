package com.cpe.springboot.arg.model;



import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.cpe.springboot.arg.FactCheck.FactCheckModel;
import com.cpe.springboot.user.model.UserModel;
import com.cpe.springboot.user.model.UserShort;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;



@Document(collection = "ArgModel")
@JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
public class ArgModel implements Comparable<ArgModel> {





		@Id
	//@GeneratedValue(strategy = GenerationType.AUTO)
	private String id;
	
	private String content;
	private HashMap<String, String> keyWords;
	private Integer numberLikes;
	private Integer numberDislikes;
	private UserShort autor;
	private HashMap<String,List<FactCheckModel>> factCheckings;
	private boolean inFavor;
	private String date;
	private Set<String> listIdLikers;
	private Set<String> listIdDisLikers;
	private String idDebat;
	

	public ArgModel(String content, HashMap<String, String> keyWords, Integer numberLikes, Integer numberDislikes,
			UserShort autor, HashMap<String, List<FactCheckModel>> idFactChecking, boolean inFavor, String date, Set<String> listIdLikers,
			Set<String> listIdDisLikers,String idDebat) {
		this.content = content;
		this.keyWords = keyWords;
		this.numberLikes = numberLikes;
		this.numberDislikes = numberDislikes;
		this.autor = autor;
		this.factCheckings = idFactChecking;
		this.inFavor = inFavor;
		this.date = date;
		this.listIdLikers = listIdLikers;
		this.listIdDisLikers = listIdDisLikers;
		this.idDebat=idDebat;
	}


	
	public ArgModel() {
		
	}

	public ArgModel(String content, HashMap<String, String> keywords, Integer numberLikes, Integer numberDislikes,
			UserShort autor, String dateFormat, HashMap<String, List<FactCheckModel>> idFactChecking, boolean inFavor, String idDebat) {
		this.content = content;
		this.keyWords=keywords;

		this.numberLikes = numberLikes;
		this.numberDislikes = numberDislikes;
		this.autor = autor;
		this.date = dateFormat;
		this.factCheckings = idFactChecking;
		this.inFavor = inFavor;
		this.listIdLikers = Collections.<String>emptySet();
		this.listIdDisLikers = Collections.<String>emptySet();
		this.idDebat=idDebat;

	}
	public ArgModel(String content, HashMap<String, String> keywords, Integer numberLikes, Integer numberDislikes,
			 String dateFormat, boolean inFavor, String idDebat) {
		this.content = content;
		this.keyWords=keywords;
		this.numberLikes = numberLikes;
		this.numberDislikes = numberDislikes;
		this.date = dateFormat;
		this.factCheckings =new HashMap<String, List<FactCheckModel>>();
		this.inFavor = inFavor;
		this.listIdLikers = Collections.<String>emptySet();
		this.listIdDisLikers = Collections.<String>emptySet();
		this.idDebat=idDebat;
     // this.autor= new UserShort();

	}
	
	public ArgModel(String content, boolean inFavor,String idDebat) {
		Calendar cal = Calendar.getInstance();
		java.util.Date date= cal.getTime();
		DateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
		String formattedDate=dateFormat.format(date);
		this.content = content;
		this.keyWords=new HashMap<String, String>();
		this.numberLikes = 0;
		this.numberDislikes = 0;
		//this.autor = new UserShort();
		this.factCheckings =new HashMap<String, List<FactCheckModel>>();
		this.inFavor = inFavor;
		this.date = formattedDate;
		this.listIdLikers =  Collections.<String>emptySet();;
		this.listIdDisLikers =  Collections.<String>emptySet();;
		this.idDebat=idDebat;

	}
	public ArgModel(String content, boolean inFavor,String idDebat,UserShort autor) {
		Calendar cal = Calendar.getInstance();
		java.util.Date date= cal.getTime();
		DateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
		String formattedDate=dateFormat.format(date);
		this.content = content;
		this.keyWords=new HashMap<String, String>();
		this.numberLikes = 0;
		this.numberDislikes = 0;
		this.autor = autor;
		this.factCheckings=new HashMap<String, List<FactCheckModel>>();
		this.inFavor = inFavor;
		this.date = formattedDate;
		this.listIdLikers =  Collections.<String>emptySet();;
		this.listIdDisLikers =  Collections.<String>emptySet();;
		this.idDebat=idDebat;
	}
	public ArgModel(String content, boolean inFavor,String idDebat,UserShort autor,HashMap<String, String> keywords) {
		Calendar cal = Calendar.getInstance();
		java.util.Date date= cal.getTime();
		DateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
		String formattedDate=dateFormat.format(date);
		this.content = content;
		this.keyWords=keywords;
		this.numberLikes = 0;
		this.numberDislikes = 0;
		this.autor = autor;
	   this.factCheckings =new HashMap<String, List<FactCheckModel>>();
		this.inFavor = inFavor;
		this.date = formattedDate;
		this.listIdLikers =  Collections.<String>emptySet();;
		this.listIdDisLikers =  Collections.<String>emptySet();;
		this.idDebat=idDebat;

	}
	public ArgModel(String content, boolean inFavor,String idDebat,HashMap<String, String> keywords) {
		Calendar cal = Calendar.getInstance();
		java.util.Date date= cal.getTime();
		DateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
		String formattedDate=dateFormat.format(date);
		this.content = content;
		this.keyWords=keywords;
		this.numberLikes = 0;
		this.numberDislikes = 0;
		//this.autor = new UserShort();
		this.factCheckings =new HashMap<String, List<FactCheckModel>>();
		this.inFavor = inFavor;
		this.date = formattedDate;
		this.listIdLikers =  Collections.<String>emptySet();;
		this.listIdDisLikers =  Collections.<String>emptySet();;
		this.idDebat=idDebat;

	}
	/*
	@OneToMany(cascade = CascadeType.ALL,
			mappedBy = "debat")
	//@OneToMany(mappedBy = "user")
	private Set<DebatModel> debatList = new HashSet<>();*/
	

	



	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}



	public Integer getNumberLikes() {
		return numberLikes;
	}

	public void setNumberLikes(Integer numberLikes) {
		this.numberLikes = numberLikes;
	}

	public Integer getNumberDislikes() {
		return numberDislikes;
	}

	public void setNumberDislikes(Integer numberDislikes) {
		this.numberDislikes = numberDislikes;
	}



	public String getDate() {
		return date;
	}

	public void setDate(String dateFormat) {
		this.date = dateFormat;
	}


	public boolean isInFavor() {
		return inFavor;
	}
	public void setInFavor(boolean inFavor) {
		this.inFavor = inFavor;
	}

	@Override
	public int compareTo(ArgModel o) {
		return getDate().compareTo(o.getDate());
	}
	public Set<String> getListIdLikers() {
		return listIdLikers;
	}
	
	public Set<String> getListIdDisLikers() {
		return listIdDisLikers;
	}
	public void setListIdDisLikers(Set<String> listIdDisLikers) {
		this.listIdDisLikers = listIdDisLikers;
	}
	public void setListIdLikers(Set<String> listIdLikers2) {
		this.listIdLikers = listIdLikers2;
		
	}



	public HashMap<String, String> getKeyWords() {
		return keyWords;
	}



	public void setKeyWords(HashMap<String, String> keyWords) {
		this.keyWords = keyWords;
	}



	public String getIdDebat() {
		return idDebat;
	}



	public void setIdDebat(String idDebat) {
		this.idDebat = idDebat;
	}


 





	public UserShort getAutor() {
		return autor;
	}



	public void setAutor(UserShort autor) {
		this.autor = autor;
	}



	public HashMap<String, List<FactCheckModel>> getFactCheckings() {
		return factCheckings;
	}



	public void setFactCheckings(
			HashMap<String, List<FactCheckModel>> factCheckings) {
		this.factCheckings = factCheckings;
	}




	
}
