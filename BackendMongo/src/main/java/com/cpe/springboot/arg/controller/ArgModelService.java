package com.cpe.springboot.arg.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.stereotype.Service;

import com.cpe.springboot.arg.model.ArgModel;



@Service
public class ArgModelService {

	public ArgModelService() {
	}


	@Autowired
	private ArgModelRepository ArgRepository;


	public List<ArgModel> getAllArgModel() {
		List<ArgModel> ArgList = new ArrayList<>();
		ArgRepository.findAll().forEach(ArgList::add);
		return ArgList;
	}
	
	public List<ArgModel> getAllArgModelRecent() {
		List<ArgModel> ArgList = new ArrayList<>();
		ArgRepository.findAll().forEach(ArgList::add);
        Collections.sort(ArgList);
        ArgList.forEach(action-> System.out.println(action.getDate()));
		return ArgList;
	}
	
	public List<ArgModel> getAllArgModelSortByLike() {
		List<ArgModel> ArgList = new ArrayList<>();
		ArgRepository.findAll().forEach(ArgList::add);
		Comparator<ArgModel> compareByLikes = (ArgModel o1, ArgModel o2) -> o1.getNumberLikes().compareTo( o2.getNumberLikes() );

        Collections.sort(ArgList,compareByLikes);
     //   ArgList.forEach(action-> System.out.println(action.getNumberLikes()));
		return ArgList;
	}

	public void addArg(ArgModel ArgModel) {
		ArgRepository.save(ArgModel);
	}

	public void updateArgRef(ArgModel ArgModel) {
		ArgRepository.save(ArgModel);

	}
	public void updateArg(ArgModel ArgModel) {
		ArgRepository.save(ArgModel);
	}
	public Optional<ArgModel> getArg(String id) {
		return ArgRepository.findById(id);
	}
	

	public void deleteArgModel(String id) {
		ArgRepository.deleteById(id);
	}

//	@EventListener(ApplicationReadyEvent.class)
	public void doInitAfterStartup() {
		Calendar cal = Calendar.getInstance();
		java.util.Date date= cal.getTime();
		DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
		String formattedDate=dateFormat.format(date);

		String[] contents= {" Je pense que la réforme des retraites est une bonne chose car la répartition des richesses",
				"La réforme des  retraites va permettre d’économiser de l’argent au niveau de l’état ",
				"Je pense que la réforme des retraites n’est pas une bonne chose car elle conserve des régimes spéciaux",
		"La réforme des  retraites va creuser les inégalités sociales"};
		String[] keywords={"retraite","inégalités","inégalités","inégalités"};
		boolean[] inFavor= {true,true,false,false};
		Random random = new Random();

		for (int i=0;i<contents.length;i++) {
			int randomInt = (int)(100.0 * Math.random());

			//ArgModel argModel =new ArgModel(contents[i], keywords[i],randomInt , randomInt, 1, formattedDate, null, inFavor[i],"1");
	//		ArgRepository.save(argModel);
		}

		//new ArgModel(content, keywords, numberLikes, numberDislikes, authorLogin, dateFormat, idFactChecking, inFavor)
	}

}
