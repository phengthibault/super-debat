package com.cpe.springboot.arg.FactCheck;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cpe.springboot.arg.controller.ArgModelService;
import com.cpe.springboot.arg.model.ArgModel;
import com.cpe.springboot.debat.controller.DebatModelService;
import com.cpe.springboot.user.model.UserModel;

@CrossOrigin
@RestController
public class FactCheckController {
	
	@Autowired
	private FactcheckService factcheckService;
	@Autowired
	private DebatModelService debatModelService;
	@Autowired
	private ArgModelService argModelService;
	@RequestMapping("/Factchecks")
	private List<FactCheckModel> getAllFactcheck() {
		return factcheckService.getAllFactcheck();

	}
	
	@RequestMapping("/Factcheck/{id}")
	private FactCheckModel getFactcheck(@PathVariable String id) {
		Optional<FactCheckModel> ruser;
		ruser= factcheckService.getFactcheck(id);
		if(ruser.isPresent()) {
			return ruser.get();
		}
		return null;

	}
	
	@RequestMapping(method=RequestMethod.POST,value="/FactCheck/{idArg}/{side}")
	public boolean addFactChecking(@RequestBody FactCheckModel factCheck,@PathVariable String idArg,@PathVariable String side) {
		ArgModel argModel=argModelService.getArg(idArg).get();
		HashMap<String, List<FactCheckModel>> factCheckings=new HashMap<String, List<FactCheckModel>>();
		List<FactCheckModel> fList=new ArrayList<FactCheckModel>();
		fList.add(factCheck);
		if(side.equals("for")) {
			factCheckings.put("For", fList);

		}else {
			factCheckings.put("Against", fList);
		}
		argModel.setFactCheckings(factCheckings);

		argModelService.updateArg(argModel);
		factcheckService.addFactcheck(factCheck);
		return true;
	}
	
	/*FactCheckModel factCheckModel2=new FactCheckModel(userShort2, "testurl", "oui",list) ;
		HashMap<String, List<FactCheckModel>> factCheckModels=new HashMap<String, List<FactCheckModel>>();

		List<FactCheckModel> fList=new ArrayList<FactCheckModel>();
		fList.add(factCheckModel2);
		factCheckModels.put("For", fList);
		factCheckModels.put("Against", fList);*/
}
