package com.cpe.springboot.arg.controller;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.CrudRepository;

import com.cpe.springboot.arg.model.ArgModel;
import com.cpe.springboot.debat.model.DebatModel;

public interface ArgModelRepository  extends MongoRepository<ArgModel, String> {
    Optional<ArgModel> findById(String id);

}
