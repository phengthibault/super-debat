package com.cpe.springboot.arg.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cpe.springboot.arg.model.ArgModel;
import com.cpe.springboot.debat.controller.DebatModelRepository;
import com.cpe.springboot.debat.controller.DebatModelService;
import com.cpe.springboot.debat.model.DebatModel;
import com.cpe.springboot.user.controller.UserService;
import com.cpe.springboot.user.model.UserModel;
import com.cpe.springboot.user.model.UserShort;



@CrossOrigin
@RestController
public class ArgRestController {


	@Autowired
	private ArgModelService argModelService;
	@Autowired
	private DebatModelService debatModelService;
	@Autowired
	private UserService userService;
	
	
	@RequestMapping("/Args")
	private List<ArgModel> getAllArgs() {
		return argModelService.getAllArgModel();

	}
	@RequestMapping("/Argsrecent")
	private List<ArgModel> getAllArgsRecent() {
		return argModelService.getAllArgModelRecent();

	}

	@RequestMapping("/GetArgsOfUser/{idUser}")
	private List<ArgModel> getAllArgsUser(@PathVariable String idUser) {
		List<ArgModel> lArgModels= argModelService.getAllArgModel();
		List<ArgModel> lArgModelsofUser= new ArrayList<ArgModel>();
		for (ArgModel argModel:lArgModels) {
			if(argModel.getAutor().getId().equals(idUser)){
				lArgModelsofUser.add(argModel);
			}
				
		}
		return lArgModelsofUser;

	}
	@RequestMapping("/ArgsSortByLikes")
	private List<ArgModel> getAllArgsSortByLike() {
		return argModelService.getAllArgModelSortByLike();

	}

	@RequestMapping("/Arg/{id}")
	private Optional<ArgModel> getArg(@PathVariable String id) {
		Optional<ArgModel> rArg;
		rArg= argModelService.getArg(id);
		return rArg;

	}


	@RequestMapping("/ArgsFor")
	private List<ArgModel> getArgsFor() {
		List<ArgModel> rArg;
		List<ArgModel> rArgFor =new ArrayList<ArgModel>();

		rArg= argModelService.getAllArgModel();
		for (ArgModel argModel: rArg) {
			if (argModel.isInFavor()) {
				rArgFor.add(argModel);
			}
		}
		return rArgFor;
	}
	@RequestMapping("/ArgsAgainst")
	private List<ArgModel> getArgsAgainst() {
		List<ArgModel> rArg;
		List<ArgModel> rArgFor =new ArrayList<ArgModel>();

		rArg= argModelService.getAllArgModel();
		for (ArgModel argModel: rArg) {
			if (!argModel.isInFavor()) {
				rArgFor.add(argModel);
			}
		}
		return rArgFor;
	}

	//add to the Debat the argument
	@RequestMapping(method=RequestMethod.POST,value="/Arg/{idAuthor}")
	public boolean addArg(@RequestBody ArgModel Arg,@PathVariable String idAuthor) {	
		boolean testArg;
		Optional<DebatModel> rDebat;		
		UserModel userModel=userService.getUser(idAuthor).get();
		Arg.setAutor(new UserShort(userModel.getId(), userModel.getLastName(), userModel.getSurName()));
		ArgModel argToAdd=new ArgModel(Arg.getContent(), Arg.isInFavor(), Arg.getIdDebat(),Arg.getAutor());

		argModelService.addArg(argToAdd);
		System.out.println(argToAdd.getIdDebat());
		rDebat= debatModelService.getDebat(argToAdd.getIdDebat());
		DebatModel debatModel=rDebat.get();
		testArg=argToAdd.isInFavor()? debatModel.getIdArgsFor().add(argToAdd.getId()):debatModel.getIdArgsAgainst().add(argToAdd.getId());
		debatModelService.updateDebat(debatModel);
		return true;
	}

	@RequestMapping(method=RequestMethod.POST,value="/Arg")
	public boolean addArg(@RequestBody ArgModel arg) {	
		ArgModel argToAdd;
		if (arg.getKeyWords()!=null) {
			argToAdd=new ArgModel(arg.getContent(), arg.isInFavor(), arg.getIdDebat(),arg.getKeyWords());

		}else {
			argToAdd=new ArgModel(arg.getContent(), arg.isInFavor(), arg.getIdDebat());

		}

		argModelService.addArg(argToAdd);
		boolean testArg;
		Optional<DebatModel> rDebat;		
		rDebat= debatModelService.getDebat(argToAdd.getIdDebat());
		if (rDebat.isEmpty()) {
			System.out.println("Debat not found");
			return false;
		}
		DebatModel debatModel=rDebat.get();
		testArg=argToAdd.isInFavor()? debatModel.getIdArgsFor().add(argToAdd.getId()):debatModel.getIdArgsAgainst().add(argToAdd.getId());
		debatModelService.updateDebat(debatModel);
		return true;

	}




	@RequestMapping(method=RequestMethod.PUT,value="/Arg/{numberLikes}")
	public void updateArg(@RequestBody ArgModel Arg) {
		argModelService.updateArg(Arg);
	}


	@RequestMapping(method=RequestMethod.GET,value="/ArgUpdateLike/{idArg}/{idLiker}")
	public boolean updateArgLikes(@PathVariable String idArg,@PathVariable String idLiker) {
		boolean ret=false;
		Optional<ArgModel> argModel=argModelService.getArg(idArg);

		if (argModel.isPresent()) {

			UserModel userModel=userService.getUser(idLiker).get();
			ArgModel arg=argModel.get();
			if (arg.getListIdLikers().add(idLiker)) {  //check if not already like by the user otherwise remove the like
				userModel.getLikesArgs().add(idArg);
				userModel.setLikesArgs(userModel.getLikesArgs());
				arg.setListIdLikers(arg.getListIdLikers());		
				arg.setNumberLikes(arg.getNumberLikes()+1);
			}
			else {
				arg.getListIdLikers().remove(idLiker);
				userModel.getLikesArgs().remove(idArg);
				arg.setNumberLikes(arg.getNumberLikes()-1);
			}	
			ret=true;		
			userService.updateUser(userModel);
			argModelService.updateArg(arg);	
		}
		return ret;

	}

	@RequestMapping(method=RequestMethod.GET,value="/ArgUpdateDisLike/{idArg}/{idLiker}")
	public boolean updateArgDisLikes(@PathVariable String idArg,@PathVariable String idLiker) {

		Optional<ArgModel> argModel=argModelService.getArg(idArg);
		if (argModel.isPresent()) {
			UserModel userModel=userService.getUser(idLiker).get();

			ArgModel arg=argModel.get();
			if (arg.getListIdDisLikers().add(idLiker)) {  //check if not already like by the user otherwise remove the like
				userModel.getDislikesArgs().add(idArg);
				userModel.setDislikesArgs(userModel.getDislikesArgs());
				arg.setListIdDisLikers(arg.getListIdDisLikers());		
				arg.setNumberDislikes(arg.getNumberDislikes()+1);
			}
			else {
				arg.getListIdDisLikers().remove(idLiker);
				userModel.getDislikesArgs().remove(idArg);
				arg.setNumberDislikes(arg.getNumberDislikes()-1);

			}	
			userService.updateUser(userModel);
			argModelService.updateArg(arg);		
			return true;

		}		
		return false;

	}


	@RequestMapping(method=RequestMethod.DELETE,value="/Arg/{id}")
	public boolean deleteUser(@PathVariable String id) {
		try {
			ArgModel argModel=argModelService.getArg(id).get();
			String idDebat=argModelService.getArg(id).get().getIdDebat();
			DebatModel debatModel=debatModelService.getDebat(idDebat).get();
			if (argModel.isInFavor()) {
				debatModel.getIdArgsFor().remove(id);


			}else {
				debatModel.getIdArgsAgainst().remove(id);
			}
			argModelService.deleteArgModel(id);
			debatModelService.updateDebat(debatModel);

			return true;}
		catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		}
	}


}
