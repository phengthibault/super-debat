package com.cpe.springboot.arg.FactCheck;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.cpe.springboot.debat.model.DebatModel;
import com.cpe.springboot.user.model.UserModel;
import com.cpe.springboot.user.model.UserShort;


@Service
public class FactcheckService {
	public FactcheckService() {
	}


	@Autowired
	private FactcheckRepository factcheckRepository;



	public List<FactCheckModel> getAllFactcheck() {
		List<FactCheckModel> factcheck = new ArrayList<>();
		factcheckRepository.findAll().forEach(factcheck::add);
		return factcheck;
	}

	public void addFactcheck(FactCheckModel Factcheck) {
		factcheckRepository.save(Factcheck);
	}

	
	public void updateFactcheck(FactCheckModel Factcheck) {
		factcheckRepository.save(Factcheck);
	}
	public Optional<FactCheckModel> getFactcheck(String id) {
		return factcheckRepository.findById(id);
	}

	public void deleteFactcheck(String id) {
		factcheckRepository.deleteById(id);
	}
	
	//@EventListener(ApplicationReadyEvent.class)
	public void doInitAfterStartup() {
		UserShort userModel =new UserShort("3", "Anonymous", "gens");

		List<String> listIdArg =new ArrayList<String>();
		listIdArg.add("1");
		FactCheckModel factCheckingModel=new FactCheckModel(userModel, "testurl", "comment", listIdArg);
		factcheckRepository.save(factCheckingModel);
		
	}
	

}
