package com.cpe.springboot.arg.FactCheck;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.cpe.springboot.arg.model.ArgModel;
import com.cpe.springboot.debat.model.DebatReference;

public interface FactcheckRepository extends MongoRepository<FactCheckModel, String> {
    Optional<FactCheckModel> findById(String id);

}
