package com.cpe.springboot.arg.FactCheck;



import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.cpe.springboot.user.model.UserModel;
import com.cpe.springboot.user.model.UserShort;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Document(collection = "FactCheckModel")
@JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
public class FactCheckModel {
	


	@Id
	private String id;
	private UserShort autor;
	private String url;
	private String content;
	private List<String> listIdArg;
	
	public FactCheckModel() {}
	
	public FactCheckModel(UserShort user, String url, String comment, List<String> listIdArg) {
		this.autor = user;
		this.url = url;
		this.content = comment;
		this.listIdArg = listIdArg;
	}


	public UserShort getAutor() {
		return autor;
	}

	public void setUser(UserShort user) {
		this.autor = user;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String comment) {
		this.content = comment;
	}



	public String getId() {
		return id;
	}



	public void setId(String id) {
		this.id = id;
	}


	public List<String> getListIdArg() {
		return listIdArg;
	}


	public void setListIdArg(List<String> listIdArg) {
		this.listIdArg = listIdArg;
	}

}
