# Super Debat


# Récuperer le git 

git clone https://gitlab.com/phengthibault/super-debat.git

# Installer les librairies python

pip install spacy
pip install re
pip install textblob
pip install profanity_check
pip install pymongo
pip install flask
pip install flask_cors
pip install numpy

python -m spacy download en_core_web_lg

# Lancement application 

Un serveur pour React : 
npm start

Un serveur pour Python : 
python Router.py

Lancement serveur Java avec Spring Boot

